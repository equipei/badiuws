<?php

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
   $settings = new admin_settingpage('local_badiuws', get_string('pluginname', 'local_badiuws'));
    $ADMIN->add('localplugins', $settings);
   
    global $CFG;
    $filepluginbadiunet=$CFG->dirroot.'/local/badiunet/version.php';
	$filepluginbadiunetformlib=$CFG->dirroot.'/local/badiunet/lib/formutil.php';
        
    if(file_exists($filepluginbadiunet) && file_exists($filepluginbadiunetformlib)){
		require_once("$CFG->dirroot/local/badiunet/lib/formutil.php"); 
		$localbadiuwsflutil= new local_badiunet_formutil();
		$localbadiuwsflutiltokenoptions=$localbadiuwsflutil->token_options();
       
	$settings->add(new admin_setting_configselect('local_badiuws/usebadiunetoken',
            get_string('usebadiunetoken', 'local_badiuws'), get_string('usebadiunetoken_desc', 'local_badiuws'), 'defaultoken', $localbadiuwsflutiltokenoptions));
    
    }
     
  //field serviceurl
    $settings->add(new admin_setting_configtext('local_badiuws/wstoken', 
		get_string('wstoken', 'local_badiuws'), get_string('wstoken_desc', 'local_badiuws'), ''));
}
