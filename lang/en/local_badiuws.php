<?php
$string['pluginname'] = 'Badiu Webservice';
                 
$string['wstoken'] ='Token de autenticação';
$string['wstoken_desc'] ='Senha de acesso ao webservice';

$string['usebadiunetoken'] ='Usar o mesmo token do plugin Badiu.Net';
$string['usebadiunetoken_desc'] ='Essa configuração permite autenticar acesso ao webservice usando o mesmo token do plugin Badiu.Net';

$string['yes']='Sim';
$string['no']='Não'; 
