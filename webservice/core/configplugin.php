<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/core/lib/configpluginlib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_coreconfigplugin extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_coreconfigpluginlib();
   }
   
    public function edit() {
       $result=array();
       $plugin=null;
       $key=null;
       $value=null;
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
           
            if(!isset($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.undefined');}
            if(empty($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.empty');}
           
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
           $value=$this->getParam()['value'];
          
       try {
            
           $result=$this->lib->edit($plugin,$key,$value);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
  
    public function save() {
       $result=array();
       $plugin=null;
       $key=null;
       $value=null;
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
           
            if(!isset($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.undefined');}
            if(empty($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.empty');}
           
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
           $value=$this->getParam()['value'];
          
       try {
            
           $result=$this->lib->save($plugin,$key,$value);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   
   public function add() {
       $result=array();
       $plugin=null;
       $key=null;
       $value=null;
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
           
            if(!isset($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.undefined');}
            if(empty($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.empty');}
           
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
           $value=$this->getParam()['value'];
          
       try {
            
           $result=$this->lib->add($plugin,$key,$value);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   public function getall() {
       $result=array();
       $plugin=null;
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
             
           $plugin=$this->getParam()['plugin'];
           
          
       try {
            
           $result=$this->lib->get_all($plugin);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   public function getvalue() {
       $result=array();
       $plugin=null;
       $key=null;
     
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
             
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
      
          
       try {
            
           $result=$this->lib->get_value($plugin,$key);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   public function getid() {
       $result=array();
       $plugin=null;
       $key=null;
     
     
     
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
             
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
      
          
       try {
            
           $result=$this->lib->get_id($plugin,$key);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
    public function existbykey() {
       $result=array();
       $plugin=null;
       $key=null;
     
     
     
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
             
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
      
          
       try {
            
           $result=$this->lib->exist_by_key($plugin,$key);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   public function existbykeyvalue() {
       $result=array();
       $plugin=null;
       $key=null;
       $value=null;
     
       global $DB;
            if(!isset($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.undefined');}
            if(empty($this->getParam()['plugin'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.plugin.empty');}
            
            if(!isset($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.undefined');}
            if(empty($this->getParam()['key'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.key.empty');}
           
            if(!isset($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.undefined');}
            if(empty($this->getParam()['value'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.value.empty');}
           
           $plugin=$this->getParam()['plugin'];
           $key=$this->getParam()['key'];
           $value=$this->getParam()['value'];
          
       try {
            
           $result=$this->lib->exist_by_key_value($plugin,$key,$value);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
    public function searchcount() {
           try {
            
           $result=$this->lib->search_count($this->getParam());
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
    public function search() {
           try {
            
           $result=$this->lib->search($this->getParam());
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}

$badiuwsdata=new local_badiuws_webservice_coreconfigplugin();
?>
