<?php

class local_badiuws_lib_coreconfigpluginlib {

    
       function edit($plugin,$key,$value) {
         
          global $CFG,$DB;
        $dto=new stdClass;
        $dto->id=$this->get_id($key);
        $dto->plugin=$plugin;
        $dto->name=$key;
        $dto->value=$value;
        return $DB->update_record('config_plugins', $dto);
    } 
     function save($plugin,$key,$value) {
          global $CFG,$DB;
     
        $dto=new stdClass;
        $dto->plugin=$plugin;
        $dto->name=$key;
        $dto->value=$value;
        return $DB->insert_record('config_plugins', $dto);
    } 
     function add($plugin,$key,$value) {
	  if($value==NULL){$value= " ";}
           if($this->exist_by_key($plugin,$key)){       
                 $this->edit($plugin,$key,$value) ;   
           }else{
                $this->save($plugin,$key,$value);
           }
      }
     function get_all($plugin) {
         global $CFG,$DB;
       $sql="SELECT name,value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$plugin."'";
        $rows=$DB->get_records_sql($sql);
      
      return $rows;
    }   
	
    function get_value($plugin,$key) {
       global $CFG,$DB;
       $sql="SELECT value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$plugin."' AND name= '".$key."'";
       $row=$DB->get_record_sql($sql);
	   
	if(!empty($row)) return $row->value;
       return null;
    }  
   
     function get_id($plugin,$key) {
          global $CFG,$DB;
       $sql="SELECT id FROM {$CFG->prefix}config_plugins WHERE  plugin='".$plugin."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->id;
    } 
    
     function exist_by_key($plugin,$key) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$plugin."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }
  function exist_by_key_value($plugin,$key,$value) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$plugin."' AND name='".$key."' AND value='".$value."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }  
  
     private function search_param($param) {
        $wsql="";
        if(isset($param['plugin']) && !empty($param['plugin'])){$wsql.=" AND = '".$param['plugin']."' ";}
        if(isset($param['name']) && $param['name']!=null){$wsql.=" AND name  LIKE '%".$param['name']."%' ";}
        if(isset($param['value'])  && !empty($param['value'])){$wsql.=" AND value LIKE '%".$param['value']."%' ";}
       return $wsql; 
    }
     public function search_count($param) {
        global $DB, $CFG;
        $wsql= $this->search_param($param);
        $sql ="SELECT  COUNT(id) AS countrecord   FROM {$CFG->prefix}config_plugins WHERE id > 0  $wsql ";
        $rows=$DB->get_record_sql($sql); 
        return $rows;
    }
    
    public function search($param) {
        global $DB, $CFG;
        $wsql= $this->search_param($param);
        $offset=0;
        $limit=100;
        if(isset($param['_offset']) && $param['_offset']!=null){$offset=$param['_offset'];}
        if(isset($param['_limit']) && $param['_limit']!=null){$limit=$param['_limit'];}
        $sql ="SELECT id,plugin,name,value FROM {$CFG->prefix}config_plugins WHERE id > 0  $wsql ";
        $rows=$DB->get_records_sql($sql,null,$offset,$limit);
        return $rows;
    }
}

?>
