
<?php 

require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
class local_badiuws_sql_command extends local_badiuws_baserole  {
    
    private $lib;

    function __construct() {
          parent::__construct();
 
    }
    
    public function exec() {
        global $DB;
        global $CFG;
        
        $offset=0;
        $limit=10;
          
             if(!isset($this->getParam()['type'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.type.undefined');}
             if(empty($this->getParam()['type'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.type.empty','Param type can not be null');}
             if($this->getParam()['type']!='S' && $this->getParam()['type']!='M'){ $this->getResponse()->danied('badiu.moodle.ws.error.param.type.is.notvalid','Param type should has value S for exec single SQL result or M to exec SQL that returns list of rows');}
            
             if(!isset($this->getParam()['query'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.query.undefined');}
             if(empty($this->getParam()['query'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.query.empty','Param query SQL can not be null');}
             if(isset($this->getParam()['offset']) && !is_numeric($this->getParam()['offset'])){$this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
             if(isset($this->getParam()['limit']) && !is_numeric($this->getParam()['limit'])){$this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
             if(isset($this->getParam()['offset']) && !empty($this->getParam()['offset'])){$offset=$this->getParam()['offset'];}
             if(isset($this->getParam()['limit']) && !empty($this->getParam()['limit'])){$limit=$this->getParam()['limit'];}
             if(empty($this->pagey)){$this->pagey=10;}
        
            $type=$this->getParam()['type'];
            $sql=$this->getParam()['query'];
            $sql=$this->changePrefixe($sql);
           
        $result=null; 
        try {
                if($type=='S'){
                          $result=$this->getRrow($sql);
		}else if($type=='M'){
			$result=$this->getRows($sql,$offset,$limit);
		}
            $this->getResponse()->accept($result);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
     
        return $id;
    }
  
    
    private function getRrow($sql) {
		global $DB, $CFG;
		 $r=$DB->get_record_sql($sql);
	        return $r; 

    }
    private function getRows($sql,$offset,$limit) {
		global $DB, $CFG;
                $r=$DB->get_records_sql($sql,null,$offset,$limit);
	        return $r; 

    }
    
       private function changePrefixe($query) {
         if(!empty($query)){
             global $CFG;
            $query=str_replace("{_pfx}",$CFG->prefix,$query);
             
         }
         return $query;
    }
    
    
}
$badiuwsdata=new local_badiuws_sql_command();


?>
