<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/grade/lib/gradelib.php");
class local_badiuws_webservice_grade extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_grade();
   }
 
   public function add() {
      
       global $DB;
           if(!isset($this->getParam()['itemid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemid.undefined');}
           if(empty($this->getParam()['itemid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemid.empty');}
           if(!is_int((int)$this->getParam()['itemid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemid.isnotnumber');}
           if(!$DB->record_exists('grade_items', array('id' => $this->getParam()['itemid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemidnotexist',$this->getParam()['itemid'].' not exist in database in the table grade_items');}
          
      
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
       
          
       try {
            
           $result=$this->lib->add($this->getParam());
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }

    
}

$badiuwsdata=new local_badiuws_webservice_grade();
?>
