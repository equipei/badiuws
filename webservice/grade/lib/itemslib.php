<?php

class local_badiuws_lib_gradeitemselib {

  public function add($param) {
       $dto = (object)$param;
       global $DB;
       if(empty($dto->timecreated)){$dto->timecreated=time();}
       return $DB->insert_record('grade_items', $dto);
    }
 
   public function exist($courseid,$itemtype,$itemmodule=null,$iteminstance=null,$itemname=null) {
      global $CFG,$DB;
      $sql=null;
      if($itemtype=='course'){
          $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}grade_items WHERE courseid=$courseid AND itemtype='".$itemtype."'";
      }else if($itemtype=='mod'){
          $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}grade_items WHERE courseid=$courseid AND itemtype='".$itemtype."' AND itemmodule='".$itemmodule."' AND iteminstance=$iteminstance";
      }else if($itemtype=='manual'){
          $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}grade_items WHERE courseid=$courseid AND itemtype='".$itemtype."' AND itemname='".$itemname."'";
      }
      $row=$DB->get_record_sql($sql);
      return $row->countrecord;
    }
    
    public function get_id_by_itemtypecourse($courseid) {
      global $CFG,$DB;
      $sql="SELECT id FROM {$CFG->prefix}grade_items WHERE courseid=$courseid AND itemtype='course'";
      $row=$DB->get_record_sql($sql);
      return $row->id;
    } 
}

?>
