<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/grade/lib/itemslib.php");
class local_badiuws_webservice_gradeitems extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_gradeitemselib();
   }
 
   public function add() {
      
       global $DB;
      $itemmodule=null;
      $iteminstance=null;
      $itemname=null;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
           if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
          
           if(!isset($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.undefined');}
           if(empty($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.empty');}
          
           if(!isset($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.undefined');}
           if(empty($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.empty');}
           
           if($this->getParam()['itemtype']=='mod'){
               if(!isset($this->getParam()['itemmodule'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemmodule.undefined');}
               if(empty($this->getParam()['itemname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemname.empty');}
               if(empty($this->getParam()['iteminstance'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.iteminstance.empty');}
               if(!is_int((int)$this->getParam()['iteminstance'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.iteminstance.isnotnumber');}
               
               $itemmodule=$this->getParam()['itemmodule'];
               $iteminstance=$this->getParam()['iteminstance'];
               $itemname=$this->getParam()['itemname'];
           }
          else  if($this->getParam()['itemtype']=='manual'){
               if(empty($this->getParam()['itemname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemname.empty');}
          }
          
          if($this->lib->exist($this->getParam()['courseid'],$this->getParam()['itemtype'],$itemmodule,$iteminstance,$itemname)){
              $this->getResponse()->danied('badiu.moodle.ws.error.param.gradeitemjustexist','duplicate row of grade item on database grade_items');
          }
       try {
            
           $result=$this->lib->add($this->getParam());
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }

  public function exist() {
      
       global $DB;
      $itemmodule=null;
      $iteminstance=null;
      $itemname=null;
      $result=null;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
           if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
          
           if(!isset($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.undefined');}
           if(empty($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.empty');}
          
           if(!isset($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.undefined');}
           if(empty($this->getParam()['itemtype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemtype.empty');}
           
           if($this->getParam()['itemtype']!='mod'){
               if(!isset($this->getParam()['itemmodule'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemmodule.undefined');}
               if(empty($this->getParam()['itemname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemname.empty');}
               if(!is_int((int)$this->getParam()['iteminstance'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.iteminstance.isnotnumber');}
               
               $itemmodule=$this->getParam()['itemmodule'];
               $iteminstance=$this->getParam()['iteminstance'];
             
           }
          else  if($this->getParam()['itemtype']!='manual'){
               if(empty($this->getParam()['itemname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.itemname.empty');}
                 $itemname=$this->getParam()['itemname'];
          }
          
         
       try {
            
          $result=$this->lib->exist($this->getParam()['courseid'],$this->getParam()['itemtype'],$itemmodule,$iteminstance,$itemname);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }  
}

$badiuwsdata=new local_badiuws_webservice_gradeitems();
?>
