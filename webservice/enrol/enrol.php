<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/enrol/lib/enrollib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_enrol extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_enrol();
   }
   
     public function add() {
         global $DB;
         $userid=$this->getParam()['userid'];
          $courseid=$this->getParam()['courseid'];
		  $groupid=null;
          $plugin='manual';
          $timestart=time();
          $timeend=0;
          $roleid=null;
          $forceupdate=0;
          $roleshorname=null;
          $enable=0;
          if(isset($this->getParam()['timestart'])){ $timestart=$this->getParam()['timestart'];}
          if(isset($this->getParam()['timeend'])){ $timeend=$this->getParam()['timeend'];}
         // if(isset($this->getParam()['roleid'])){ $roleid=$this->getParam()['roleid'];}
          if(isset($this->getParam()['plugin'])){ $plugin=$this->getParam()['plugin'];}
          if(isset($this->getParam()['forceupdate'])){ $forceupdate=$this->getParam()['forceupdate'];}
          if(isset($this->getParam()['roleshorname'])){ $roleshorname=$this->getParam()['roleshorname'];}
          if (array_key_exists('enable',$this->getParam())) { $enable=$this->getParam()['enable'];}
		   
          if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
         if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
         if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
         if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
        
         if(!isset($this->getParam()['roleshorname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.roleshorname.undefined');}
         $roleid=$DB->get_field('role', 'id', array('shortname'=>$roleshorname));
         if(empty($roleid)){ 
             $this->getResponse()->danied('badiu.moodle.ws.error.param.roleshornamenotexist',$this->getParam()['roleshorname'].' not exist in database in the table role');
          }

   
         
         if(empty($this->getParam()['plugin'])){$plugin='manual';}
         
         
         if(!empty($this->getParam()['timestart'])){ 
             if(!is_int((int)$this->getParam()['timestart'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart.isnotnumber');}
         }else{$timestart=time();}  
         
         if(!empty($this->getParam()['timeend'])){ 
             if(!is_int((int)$this->getParam()['timeend'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend.isnotnumber');}
         }else{$timeend=0;} 
          
		 
		  if(!empty($this->getParam()['groupid'])){ 
             if(!is_int((int)$this->getParam()['groupid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.groupid.isnotnumber');}
			 $groupid=$this->getParam()['groupid'];
         }
		 
          $resultenrol=null;
          $resultrole=null;

          $status=1;
          if($enable==0){$status=1;}
          else if($enable==1){$status=0;}
          try {
              $enrolid=$this->lib->get_enrol_id($courseid,$plugin);
              $contextid=$this->lib->get_course_context($courseid);
           //add enrol
		      	$enrolparam=array('enrolid'=>$enrolid,'userid'=>$userid,'timestart'=>$timestart,'timeend'=>$timeend,'forceupdate'=>$forceupdate,'groupid'=>$groupid,'status'=>$status);
			      $releparam=array('contextid'=>$contextid,'userid'=>$userid,'roleid'=>$roleid,'forceupdate'=>$forceupdate); 
         
            $resultenrol=$this->lib->add_enrol($enrolparam);
           $resultrole=$this->lib->add_role($releparam);
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        $result=array('enrol'=>$resultenrol,'role'=>$resultrole);
        return $result;
      }
      /*
     private function addCheck() {
       global $DB;
       global $CFG;
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         
           
          if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
         if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
       
         //check exist login
        $existlogin=$DB->record_exists('user', array('username' => $this->getParam()['username'], 'mnethostid' => $CFG->mnet_localhost_id));
    
        if ($existlogin) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateusername','username '. $this->getParam()['username'].' just exist in database');
            }
        
        //exist email
         $existemail=$DB->record_exists('user', array('email' => $this->getParam()['email'], 'mnethostid' => $CFG->mnet_localhost_id));
         if ($existemail) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateemail','E-mail '. $this->getParam()['email'].' just exist in database');
            }
     
    } 
   */

     public function addincoursesofcategory() {
          global $DB;
          
          $categoryid=$this->getParam()['categoryid'];
          $userid=$this->getParam()['userid'];
          $roleid=$this->getParam()['roleid'];
          $plugin=$this->getParam()['plugin'];
          $timestart=$this->getParam()['timestart'];
          $timeend=$this->getParam()['timeend'];
          $limit=$this->getParam()['limit'];
       
      
          $plugin='manual';
          
          $timeend=0;
          
          
         if(!isset($this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');}
         if(empty($this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.empty');}
         if(!is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
         if(!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist',$this->getParam()['categoryid'].' not exist in database in the table course_categories');}
          
          
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
         
         if(!empty($this->getParam()['roleid'])){ 
             if(!is_int((int)$this->getParam()['roleid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.roleid.isnotnumber');}
            if(!$DB->record_exists('role', array('id' => $this->getParam()['roleid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.roleidnotexist',$this->getParam()['roleid'].' not exist in database in the table role');}
         }else{$roleid=5;}
         
         if(!empty($this->getParam()['timestart'])){ 
             if(!is_int((int)$this->getParam()['timestart'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart.isnotnumber');}
         }else{$timestart=time();}  
         
         if(!empty($this->getParam()['timeend'])){ 
             if(!is_int((int)$this->getParam()['timeend'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend.isnotnumber');}
         }else{$timeend=0;}  
         
         if(!empty($this->getParam()['limit'])){ 
             if(!is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
         }else{$limit=500;}  
         
         $param=array();
         $param['categoryid']=$categoryid;
         $param['userid']=$userid;
         $param['roleid']=$roleid;
         $param['plugin']=$plugin;
         $param['timestart']=$timestart;
         $param['timeend']=$timeend;
         $param['limit']=$limit;
          
          $result=null;
       
          try {
              $result=$this->lib->add_enrol_in_courses_of_category($param); 
             
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
      
        return $result;
      }
      
    public function getenablemethodenrolsbycourseid() {
         global $DB;
         $courseid=$this->getParam()['courseid'];
          if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
         if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
         if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
         if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
        
       
          
          $result=null;
      
          try {
               $result=$this->lib->get_enable_methodenrols_by_courseid($courseid);
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
      
       public function getenrolscount() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
        
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
          
          $result=null;
      
          try {
               $result=$this->lib->get_enrols_count($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
       public function getenrols() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
         
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
     
       
          
          $result=null;
      
          try {
               $result=$this->lib->get_enrols($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
      
       public function getcompletedcoursescount() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
        
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
          
          $result=null;
      
          try {
               $result=$this->lib->get_completedcourses_count($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
       public function getcompletedcourses() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
         
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
     
       
          
          $result=null;
      
          try {
               $result=$this->lib->get_completedcourses($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
        public function getcourseswihtaccesscount() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
         
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
     
       
          
          $result=null;
      
          try {
               $result=$this->lib->get_courses_wihtaccess_count($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
      
        public function getcourseswihtaccess() {
         global $DB;
   
         if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
         if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
         if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
         
        if(!empty($this->getParam()['courseid']) && !is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
        if(!empty($this->getParam()['enrolmethodstatus']) && !is_int((int)$this->getParam()['enrolmethodstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolmethodstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolstatus']) && !is_int((int)$this->getParam()['enrolstatus'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolstatus.isnotnumber');}
        if(!empty($this->getParam()['enrolrole']) && !is_int((int)$this->getParam()['enrolrole'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.enrolrole.isnotnumber');}
        if(!empty($this->getParam()['categoryid']) && !is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
        if(!empty($this->getParam()['coursevisible']) && !is_int((int)$this->getParam()['coursevisible'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursevisible.isnotnumber');}
        
        if(!empty($this->getParam()['timestart1']) && !is_int((int)$this->getParam()['timestart1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart1.isnotnumber');}
        if(!empty($this->getParam()['timestart2']) && !is_int((int)$this->getParam()['timestart2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timestart2.isnotnumber');}
        if(!empty($this->getParam()['timeend1']) && !is_int((int)$this->getParam()['timeend1'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend1.isnotnumber');}
        if(!empty($this->getParam()['timeend2']) && !is_int((int)$this->getParam()['timeend2'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timeend2.isnotnumber');}
        
        if(!empty($this->getParam()['offset']) && !is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
        if(!empty($this->getParam()['limit']) && !is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
     
       
          
          $result=null;
      
          try {
               $result=$this->lib->get_courses_wihtaccess($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
        return $result;
      }
}

$badiuwsdata=new local_badiuws_webservice_enrol();
?>
