<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/enrol/lib/changedatelib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_enrol_changedate extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_enrol_changedate();
   }
   
   public function countemptytimestart() {
       $result=null;
       try {
             $result=$this->lib->count_empty_timestart();
          } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }  
        return $result;
   }

   public function countemptytimeend() {
       $result=null;
       try {
             $result=$this->lib->count_empty_timeend();
          } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }  
        return $result;
   }


     public function fillemptytimestartwithtimecreated() {
         global $DB;
         $limit=500;
     
         if(!empty($this->getParam()['limit'])){ 
             if(!is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
             $limit=$this->getParam()['limit'];
         }
          $result=null;
          try {
              $result=$this->lib->fill_empty_timestart_with_timecreated($limit);
          } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
        return $result;
      }
   
    public function fillemptytimeendwithtimestart() {
         global $DB;
         $limit=500;
         $dayadd=$this->getParam()['dayadd'];

         if(!empty($this->getParam()['limit'])){ 
             if(!is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
             $limit=$this->getParam()['limit'];
         }
           if(!isset($this->getParam()['dayadd'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.dayadd.undefined');}
         if(empty($this->getParam()['dayadd'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.dayadd.empty');}
       
          $result=null;
          try {
              $result=$this->lib->fill_empty_timeend_with_timestart($dayadd,$limit);
          } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
        return $result;
      }
}

$badiuwsdata=new local_badiuws_webservice_enrol_changedate();
?>
