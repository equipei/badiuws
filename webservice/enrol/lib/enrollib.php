<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
require_once("$CFG->dirroot/local/badiuws/lib/baselib.php");
class local_badiuws_lib_enrol extends local_badiuws_baselib{
	function __construct() {
		parent::__construct();
	}
  public function get_course_context($courseid) {
        global $DB, $CFG;
	$sql ="SELECT id FROM {$CFG->prefix}context WHERE instanceid=$courseid AND contextlevel=50";
	$row=$DB->get_record_sql($sql); 
        return $row->id;
      
    }
  
    public function get_enrol_id($courseid,$plugin='manual') {
        global $DB, $CFG;
	$sql ="SELECT id FROM {$CFG->prefix}enrol WHERE courseid=$courseid AND enrol='".$plugin."' AND status=0" ;
	$row=$DB->get_record_sql($sql); 
        return $row->id;
    }
 
    public function add_role($param) {
		   $contextid=$this->getUtildata()->getVaueOfArray($param,'contextid');
		   $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		   $roleid=$this->getUtildata()->getVaueOfArray($param,'roleid');
           $forceupdate=$this->getUtildata()->getVaueOfArray($param,'forceupdate');
          global $DB;
          if($forceupdate){
            if($this->exist_role_without_role($contextid,$userid)){
            $r= $this->update_role($contextid,$userid,$roleid);
            return $r;
            }
         }
            if($this->exist_role($contextid,$userid,$roleid)){return -1;}
            $dto=  new stdClass();
            $dto->roleid=$roleid;
            $dto->contextid=$contextid;
            $dto->userid=$userid;
            return $DB->insert_record('role_assignments', $dto);
    }
     public function add_enrol($param) {
		  $enrolid=$this->getUtildata()->getVaueOfArray($param,'enrolid');
		  $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
		  $timestart=$this->getUtildata()->getVaueOfArray($param,'timestart');
		  $timeend=$this->getUtildata()->getVaueOfArray($param,'timeend');
		  $forceupdate=$this->getUtildata()->getVaueOfArray($param,'forceupdate');
		  $groupid=$this->getUtildata()->getVaueOfArray($param,'groupid');
          $status=$this->getUtildata()->getVaueOfArray($param,'status');
          
		 $this->add_group_members($param);
        global $DB;
        //review filter status to check if exist and update status
        if($this->exist_enrol($userid,$enrolid,$status)){
           if($forceupdate){$this->update_timestart_timeend($userid,$enrolid,$status,$timestart,$timeend);}
			return -1;
		}
        $result=null;
        $dto=new stdClass();
        $dto->status=$status;
        $dto->enrolid=$enrolid;
        $dto->userid=$userid;
        $dto->timestart=$timestart;
        $dto->timeend=$timeend;
        $dto->timecreated=time();
        $result=$DB->insert_record('user_enrolments', $dto);
       
         return  $result;
    }
    
    public function exist_role($contextid,$userid,$roleid) {
        global $CFG,$DB;
	$sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}role_assignments WHERE contextid=$contextid AND userid=$userid AND roleid=$roleid ";
	$r=$DB->get_record_sql($sql);
	return $r->countrecord;
    }
    public function exist_role_without_role($contextid,$userid) {
        global $CFG,$DB;
	    $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}role_assignments WHERE contextid=$contextid AND userid=$userid";
	    $r=$DB->get_record_sql($sql);
	    return $r->countrecord;
    }

    function update_role($contextid,$userid,$roleid) {
        if(empty($userid)){return null;}
        if(empty($roleid)){return null;}
        if(empty($contextid)){return null;}
        global $DB, $CFG;
		$fparam=array('contextid'=>$contextid,'userid'=>$userid,'roleid'=>$roleid,'timemodified'=>time());
        $sql = "UPDATE {$CFG->prefix}role_assignments SET roleid=:roleid,timemodified=:timemodified  WHERE contextid=:contextid AND userid=:userid";
        $r = $DB->execute($sql,$fparam);
        return $r;
}
   function update_timestart_timeend($userid,$enrolid,$status=0,$timestart=0,$timeend=0) {
        if(empty($userid)){return null;}
        if(empty($enrolid)){return null;}
        global $DB, $CFG; 
		$fparam=array('timestart'=>$timestart,'timeend'=>$timeend,'enrolid'=>$enrolid,'userid'=>$userid,'status'=>$status,'timemodified'=>time());
        $sql = "UPDATE {$CFG->prefix}user_enrolments SET timestart=:timestart,timeend=:timeend,status=:status ,timemodified=:timemodified  WHERE enrolid=:enrolid AND userid=:userid";
        $r = $DB->execute($sql,$fparam);
        return $r;
}
  public function exist_enrol($userid,$enrolid) {
      global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}user_enrolments WHERE userid=$userid AND enrolid=$enrolid ";
	 $reu=$DB->get_record_sql($sql);
         if($reu->countrecord==0){
            return FALSE;
        }
        return TRUE;
    }
    
function  add_group_members($param){
	 global $CFG,$DB;
	
	 $groupid=$this->getUtildata()->getVaueOfArray($param,'groupid');
	 $userid=$this->getUtildata()->getVaueOfArray($param,'userid');
	 if(empty($groupid)){return null;}
	 if(empty($userid)){return null;}
	 $exist= $DB->record_exists('groups_members', array('groupid'=>$groupid,'userid'=>$userid));
	 $r=null;
	 if(!$exist){
		 $fparam=new stdClass();
		 $fparam->userid=$userid;
		 $fparam->groupid=$groupid;
		 $fparam->timecreated=time();
		$r= $DB->insert_record('groups_members', $fparam);
	 }
	 return $r;
}
   public function add_enrol_in_courses_of_category($param) {
        global $DB, $CFG;
        $categoryid=$param['categoryid'];
        $userid=$param['userid'];
        $roleid=$param['roleid'];
        $plugin=$param['plugin'];
        $timestart=$param['timestart'];
        $timeend=$param['timeend'];
        $limit=$param['limit'];
       
                
        $lastid=0;
        $cont=0;
        $proces=0;
        $paging=$limit/100;
        for ($index = 0; $index <  $paging; $index++) {
             //get list of couses id
            $courses=$this->get_courseid_by_cagegoryid($categoryid,$lastid,$limit);
            $contcourse=0;
            foreach ($courses as $course) {
                $courseid=$course->id;
                
                $enrolid=$this->get_enrol_id($courseid,$plugin);
                $contextid=$this->get_course_context($courseid);
          
               //add enrol
              $resultu=$this->add_enrol($enrolid,$userid,$timestart,$timeend);
               $result=$this->add_role($contextid,$userid,$roleid);
               if($result > 0 && $resultu  > 0){$proces++;}
               $cont++;
               $lastid=$courseid;
               $contcourse++;
            }
            if($contcourse==0){break;}
            if($cont>=$limit){break;}
            
        }
       return $proces; 
   }  
 
  public function get_courseid_by_cagegoryid($categoryid,$lastid,$limit=100) {
         global $DB;
        global $CFG;
        $offset=0;
        $sql = "SELECT  id FROM {$CFG->prefix}course WHERE id > $lastid AND category=$categoryid ORDER BY id ";
        $rows = $DB->get_records_sql($sql,null,$offset,$limit);
        return $rows;
    }
 
    
public function get_enable_methodenrols_by_courseid($courseid) {
        global $DB, $CFG;
	$sql ="SELECT id,enrol,name FROM {$CFG->prefix}enrol WHERE courseid=$courseid AND status=0";
	$rows=$DB->get_records_sql($sql); 
        $result=array();
       
        foreach ($rows as $row) {
            $name=$row->enrol;
            $iraw=array();
            if(!empty($row->name)){$name.="/".$row->name;}
            $iraw['id']=$row->id;
            $iraw['name']=$name;
            array_push($result,$iraw);
        }
        return $result;
    }
    
      
              
     private function get_enrols_wsql($param) {
          $util=new local_badiuws_util();
          $wsql="";
          $course=$util->getVaueOfArray($param,'course');
          if(!empty($course)){$wsql=" AND LOWER(CONCAT(c.id,c.fullname,c.shortname)) LIKE '".$course."' ";}
          
          $courseid=$util->getVaueOfArray($param,'courseid');
          if(!empty($courseid)){$wsql=" AND c.id= $courseid";}
           
          $coursename=$util->getVaueOfArray($param,'coursename');
          if(!empty($coursename)){$wsql=" AND LOWER(CONCAT(c.fullname)) LIKE '".$coursename."' ";}
          
          $courseshortname=$util->getVaueOfArray($param,'courseshortname');
          if(!empty($courseshortname)){$wsql=" AND LOWER(CONCAT(c.shortname)) LIKE '".$courseshortname."' ";}
         
                
          $coursevisible=$util->getVaueOfArray($param,'coursevisible');
          if($coursevisible >=0 ){$wsql=" AND c.visible=$coursevisible ";}
          
          $categoryid=$util->getVaueOfArray($param,'categoryid');
          if(!empty($categoryid)){$wsql=" AND c.category= $categoryid";}
         
          $enrolrole=$util->getVaueOfArray($param,'enrolrole');
          if($enrolrole >=0 ){$wsql=" AND rs.roleid = $enrolrole ";}
          
           $roleshortname=$util->getVaueOfArray($param,'roleshortname');
          if(!empty($roleshortname)){$wsql="  AND r.shortname = '".$roleshortname."' ";}
          
          $enrolmethod=$util->getVaueOfArray($param,'enrolmethod');
         if(!empty($enrolmethod)){$wsql="  AND en.enrol = '".$enrolmethod."' ";}
         
          $enrolstatus=$util->getVaueOfArray($param,'enrolstatus');
          if($enrolrole >=0 ){$wsql=" AND  ue.status = $enrolstatus ";}
          
           $enrolmethodstatus=$util->getVaueOfArray($param,'enrolmethodstatus');
          if($enrolmethodstatus >=0 ){$wsql="  AND en.status = $enrolmethodstatus ";}
          
          $timestart1=$util->getVaueOfArray($param,'timestart1');
          if($timestart1 > 0 ){$wsql="  AND ue.timestart >= $timestart1 ";}
          
          $timestart2=$util->getVaueOfArray($param,'timestart2');
          if($timestart2 > 0 ){$wsql="  AND ue.timestart <= $timestart2 ";}
          
          $timeend1=$util->getVaueOfArray($param,'timeend1');
          if($timeend1 > 0 ){$wsql="  AND ue.timeend >= $timeend1 ";}
          
          $timeend2=$util->getVaueOfArray($param,'timeend2');
          if($timeend2 > 0 ){$wsql="  AND ue.timeend <= $timeend2 ";}
          
     }
     public function get_enrols_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT COUNT(ue.id) AS countrecord  FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid   INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE e.contextlevel=50 AND rs.userid=ue.userid AND rs.userid=$userid $wsql";
        $r = $DB->get_record_sql($sql);
        $r =$r->countrecord;
        return $r;
   } 
   
    public function get_enrols($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        
         if(!empty($offset)){$offset=0;}
        if(!is_int((int)$offset)){$offset=0;}
        
        if(!empty($limit)){$limit=10;}
        if(!is_int((int)$limit)){$limit=10;}
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT ue.id,rs.id AS roleassignmentsid,c.id AS courseid,c.fullname AS course,ct.name AS category,ct.id AS categoryid,r.name AS rolename,r.shortname AS roleshortname,en.status AS methodstatus,en.enrol AS methodplugin,ue.status AS enrolstatus,ue.timestart,ue.timeend    FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE e.contextlevel=50 AND rs.userid=ue.userid AND rs.userid=$userid $wsql";
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
   
   public function get_completedcourses_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT COUNT(DISTINCT rs.id) AS countrecord  FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id  INNER JOIN {$CFG->prefix}course_completions p ON p.course=c.id  WHERE e.contextlevel=50 AND rs.userid=ue.userid AND p.userid=rs.userid AND p.timecompleted > 0 AND rs.userid=$userid $wsql";
        $r = $DB->get_record_sql($sql);
        $r->countrecord;
   } 
   
   public function get_completedcourses($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        
         if(!empty($offset)){$offset=0;}
        if(!is_int((int)$offset)){$offset=0;}
        
        if(!empty($limit)){$limit=10;}
        if(!is_int((int)$limit)){$limit=10;}
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT DISTINCT rs.id AS roleassignmentsid,c.id AS courseid,c.fullname AS course,ct.name AS category,ct.id AS categoryid,r.name AS rolename,r.shortname AS roleshortname,en.status AS methodstatus,en.enrol AS methodplugin,ue.status AS enrolstatus,ue.timestart,ue.timeend    FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id  INNER JOIN {$CFG->prefix}course_completions p ON p.course=c.id  WHERE e.contextlevel=50 AND rs.userid=ue.userid AND p.userid=rs.userid AND p.timecompleted > 0 AND rs.userid=$userid $wsql";
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
   
   public function get_courses_wihtaccess_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
               
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT COUNT(DISTINCT rs.id) AS countrecord   FROM {$CFG->prefix}logstore_standard_log l INNER JOIN {$CFG->prefix}role_assignments rs ON  rs.userid=l.userid INNER JOIN {$CFG->prefix}context e ON (rs.contextid=e.id AND l.courseid=e.instanceid) INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE e.contextlevel=50 AND rs.userid=ue.userid  AND l.userid=ue.userid AND l.courseid=c.id AND rs.userid=$userid $wsql";
        $r = $DB->get_record_sql($sql);
        $r= $r->countrecord;
        return $r;
   } 
   
   public function get_courses_wihtaccess($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        
         if(!empty($offset)){$offset=0;}
        if(!is_int((int)$offset)){$offset=0;}
        
        if(!empty($limit)){$limit=10;}
        if(!is_int((int)$limit)){$limit=10;}
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT ue.id,rs.id AS roleassignmentsid,c.id AS courseid,c.fullname AS course,ct.name AS category,ct.id AS categoryid,r.name AS rolename,r.shortname AS roleshortname,en.status AS methodstatus,en.enrol AS methodplugin,ue.status AS enrolstatus,ue.timestart,ue.timeend,COUNT(l.id) AS countaccess   FROM {$CFG->prefix}logstore_standard_log l INNER JOIN {$CFG->prefix}role_assignments rs ON  rs.userid=l.userid INNER JOIN {$CFG->prefix}context e ON (rs.contextid=e.id AND l.courseid=e.instanceid) INNER JOIN {$CFG->prefix}role r ON rs.roleid=r.id  INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON en.id=ue.enrolid  INNER JOIN {$CFG->prefix}course c ON c.id=en.courseid  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE e.contextlevel=50 AND rs.userid=ue.userid AND l.userid=ue.userid AND l.courseid=c.id AND rs.userid=$userid $wsql GROUP BY ue.id,rs.id,c.id,c.fullname,ct.name,ct.id,r.name,r.shortname,en.status,en.enrol,ue.status,ue.timestart,ue.timeend ";
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   }
   
    
   public function get_last_courses_wihtaccess($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        
         if(!empty($offset)){$offset=0;}
        if(!is_int((int)$offset)){$offset=0;}
        
        if(!empty($limit)){$limit=10;}
        if(!is_int((int)$limit)){$limit=10;}
        
        $wsql=$this->get_enrols_wsql($param);
        $sql="SELECT c.id AS courseid,c.fullname AS course,ct.name AS category,ct.id AS categoryid,MAX(l.timecreated) FROM {$CFG->prefix}logstore_standard_log l INNER JOIN {$CFG->prefix}course c ON c.id=l.courseid INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE l.userid=$userid $wsql GROUP BY c.id,c.fullname,ct.name,ct.id ORDER BY MAX(l.timecreated) DESC";
       
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   }  
}

?>
