<?php

class local_badiuws_lib_enrol_changedate {

    public function count_empty_timestart() {
         global $CFG,$DB;
	$sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}user_enrolments WHERE timestart =0 ";
	$r=$DB->get_record_sql($sql);
	return $r->countrecord;
     }
   
    
   public function get_enrol_timecreated_empty_timestart($limit=500) {
         global $CFG,$DB;
         $sql="SELECT id,timecreated FROM {$CFG->prefix}user_enrolments WHERE timestart = 0 ";
	 $rows=$DB->get_records_sql($sql,null,0, $limit);
	return $rows;
     }
     
  public function fill_empty_timestart_with_timecreated($limit=500) {
         global $CFG,$DB;
         $enerols=$this->get_enrol_timecreated_empty_timestart($limit);
         $result=0;
         foreach ($enerols as $enrol) {
             $timestart=$enrol->timecreated;
             $id=$enrol->id;
              $timemodified=time();
              $sql = "UPDATE {$CFG->prefix}user_enrolments SET timestart=$timestart,timemodified=$timemodified WHERE id = $id ";
             $r= $DB->execute($sql);
             if($r){$result++;}
         }
         return $result;
     }


  public function count_empty_timeend() {
         global $CFG,$DB;
	$sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}user_enrolments WHERE timeend =0";
	$r=$DB->get_record_sql($sql);
	return $r->countrecord;
     }     
  
     
   public function get_enrol_timestart_empty_timeend($limit=500) {
         global $CFG,$DB;
         $sql="SELECT id,timestart FROM {$CFG->prefix}user_enrolments WHERE timestart >  0 AND timeend = 0 ";
	 $rows=$DB->get_records_sql($sql,null,0, $limit);
	return $rows;
     }
   
   
    public function fill_empty_timeend_with_timestart($dayadd=90,$limit=500) {
         global $CFG,$DB;
         $enerols=$this->get_enrol_timestart_empty_timeend($limit);
         $result=0;
         foreach ($enerols as $enrol) {
             $timeend=$enrol->timestart+($dayadd*86400);
             $id=$enrol->id;
             $timemodified=time();
              $sql = "UPDATE {$CFG->prefix}user_enrolments SET timeend=$timeend,timemodified=$timemodified WHERE id = $id ";
             $r= $DB->execute($sql);
             if($r){$result++;}
         }
         return $result;
     }
     
    
    function updateall_enrolwithtimestartnull($limit){
        $courses=$this->get_courses_withenroltimestartnull($limit);
        $cont=0;
        foreach ($courses as $course) {
            $result=change_enrolwithnulltimestar($course->enrolid, $course->startdate);
            if($result){$cont++;}
      
        }
     return $cont;
}

    function get_courses_withenroltimestartnull($limit) {
        global $DB, $CFG;
        $sql = "SELECT DISTINCT c.id,ue.enrolid,c.startdate,c.fullname FROM {$CFG->prefix}enrol e INNER JOIN {$CFG->prefix}course c ON c.id= e.courseid INNER JOIN {$CFG->prefix}user_enrolments ue ON e.id=ue.enrolid WHERE ue.timestart =0 OR ue.timestart IS NULL";
        $rows = $DB->get_records_sql($sql,null,0,$limit);
        return $rows;
    }

    function change_enrolwithnulltimestar($enrolid, $timestart) {
        if(empty($enrolid)){return null;}
        if(empty($timestart)){return null;}
        global $DB, $CFG;
        $sql = "UPDATE {$CFG->prefix}user_enrolments SET timestart=$timestart WHERE enrolid=$enrolid AND (timestart=0 OR timestart IS NULL)";
        $r = $DB->execute($sql);
        return $r;
}
}

?>
