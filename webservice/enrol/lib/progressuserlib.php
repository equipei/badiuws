<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
class local_badiuws_lib_progressuser {

     public function get_coursesid_enroled($userid) {
        global $DB, $CFG;
	$sql ="SELECT c.id, c.fullname AS name FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course c ON c.id=e.instanceid WHERE e.contextlevel=50 AND rs.userid=$userid " ;
	$rows=$DB->get_records_sql($sql); 
        return $rows;
    }
    
     public function get_coursesid_completed($userid) {
        global $DB, $CFG;
	$sql ="SELECT course AS courseid FROM  {$CFG->prefix}course_completions WHERE userid=$userid AND timecompleted > 0 " ;
	$rows=$DB->get_records_sql($sql); 
        return $rows;
    }
 
    public function get_activities_anebleprogress($userid) {
        global $DB, $CFG;
	$sql ="SELECT cm.course AS courseid,COUNT(DISTINCT cm.id) AS countactivityanebleprogress FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course_modules cm ON cm.course=e.instanceid WHERE e.contextlevel=50 AND cm.completion > 0  AND cm.visibleold=1 AND cm.deletioninprogress=0 AND rs.userid=$userid  GROUP BY cm.course" ;
	$rows=$DB->get_records_sql($sql); 
        return $rows;
    }
   
     public function get_activities_completed($userid) {
        global $DB, $CFG;
	$sql ="SELECT cm.course AS courseid,COUNT(cmp.id) AS countactivitycompleted FROM {$CFG->prefix}course_modules_completion cmp INNER JOIN {$CFG->prefix}course_modules cm ON cmp.coursemoduleid = cm.id WHERE  cm.completion > 0 AND cmp.completionstate > 0 AND cm.visibleold=1 AND cm.deletioninprogress=0 AND cmp.userid=$userid GROUP BY  cm.course" ;
	$rows=$DB->get_records_sql($sql); 
        return $rows;
    }
     public function get_activities_completed_by_courseid($userid,$courseid) {
        global $DB, $CFG;
	$sql ="SELECT cm.id FROM {$CFG->prefix}course_modules_completion cmp INNER JOIN {$CFG->prefix}course_modules cm ON cmp.coursemoduleid = cm.id WHERE  cm.completion > 0 AND cmp.completionstate > 0  AND cm.deletioninprogress=0 AND cmp.userid=$userid  AND cm.course = $courseid " ;
	$rows=$DB->get_records_sql($sql); 
        return $rows;
    }
    public function get_courses_progress($userid) {
        $util=new local_badiuws_util();
        $progress = array();
            
        //get courses
        $listenrols=$this->get_coursesid_enroled($userid);
        if(!empty($listenrols)){
            foreach ($listenrols as $row) {
               $id=$row->id;
               $name=$row->name;
               $progress[$id]=array('id'=>$id,'name'=>$name,'progress'=>null,'activitiesenableprogrss'=>null,'activitiescompleted'=>null,'type'=>null);
             }
        }
        
        //get couses completed
        $listcompleted=$this->get_coursesid_completed($userid);
        if(!empty($listcompleted)){
            foreach ($listcompleted as $row) {
               $id=$row->courseid;
               $progressitem=$util->getVaueOfArray($progress,$id);
               $progressitem['progress']=100;
               $progressitem['type']='readcourseompletions';
               $progress[$id]=$progressitem;
             }
        }
        
        //get activities enable progress
        $activitiesenableprogrss=$this->get_activities_anebleprogress($userid);
        if(!empty($activitiesenableprogrss)){
            foreach ($activitiesenableprogrss as $row) {
               $id=$row->courseid;
               $count=$row->countactivityanebleprogress;
               $progressitem=$util->getVaueOfArray($progress,$id);
               $progressitem['activitiesenableprogrss']=$count;
               $progress[$id]=$progressitem;
             }
        }
        
        //get activities completed
        $activitiescompleted=$this->get_activities_completed($userid);
        if(!empty($activitiescompleted)){
            foreach ($activitiescompleted as $row) {
               $id=$row->courseid;
               $count=$row->countactivitycompleted;
               $progressitem=$util->getVaueOfArray($progress,$id);
               $progressitem['activitiescompleted']=$count;
               $progress[$id]=$progressitem;
             }
        }
      
       //make 
        $newlist= array();
       foreach( $progress as $key => $item) {
            $activitiesenableprogrss=$util->getVaueOfArray($item,'activitiesenableprogrss');  
            $activitiescompleted=$util->getVaueOfArray($item,'activitiescompleted');  
            $type=$util->getVaueOfArray($item,'type');  
            if($type!='readcourseompletions'){
                $perc=0;
                if($activitiesenableprogrss!=null && $activitiescompleted !=null && $activitiesenableprogrss > $activitycompleted ){
                    $perc=$activitiescompleted*100/$activitiesenableprogrss;
                }
                $item['progress']=$perc;
            }
           $newlist[$key]=$item;
       }
    
       return $newlist;
    }
}

?>
