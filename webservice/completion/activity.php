<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/page/lib/pagelib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_page extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_page();
   }
   
    public function getcontentbyid() {
       $result=array();
       $id=null;
     
       global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
          $id=$this->getParam()['id'];
          
       try {
            
           $result=$this->lib->get_content_by_id($id);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
  
  public function getcontentbycmid() {
       $result=array();
       $id=null;
     
       global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
          $id=$this->getParam()['id'];
          
       try {
            
           $result=$this->lib->get_content_by_cmid($id);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}

$badiuwsdata=new local_badiuws_webservice_page();
?>
