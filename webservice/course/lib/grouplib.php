<?php
class local_badiuws_lib_group {

    function get_list($courseid,$name=null,$offset,$limit) {
         global $DB;
         global $CFG;
        $wsql="";
         if(!empty($courseid)){
           $wsql=" AND courseid= $courseid ";
        }
        if(!empty($name)){
            $wsql.=" AND LOWER(CONCAT(id,name))  LIKE '%".strtolower($name)."%'";
        }
        $sql = "SELECT  id,name FROM {$CFG->prefix}groups  WHERE  id > 0 $wsql";
     
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
    }
    
   private function search_wsql($param) {
          $util=new local_badiuws_util();
          $wsql="";
          $courseid=$util->getVaueOfArray($param,'courseid');
          if($courseid > 0 ){
             $wsql.=" g.courseid = $courseid ";
          }
          
          $gsearch=$util->getVaueOfArray($param,'gsearch');
          if(!empty($gsearch)){
             $wsql.=" AND LOWER(CONCAT(g.id,g.name))  LIKE '%".strtolower($gsearch)."%'";
          }
      
          $name=$util->getVaueOfArray($param,'name');
          if(!empty($name)){
             $wsql.=" AND LOWER(g.name)  LIKE '%".strtolower($name)."%'";
          }
          return  $wsql;
     }
     public function search_count($param) {
        $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $wsql=$this->search_wsql($param);
        $sql="SELECT COUNT(DISTINCT g.id) AS countrecord  FROM {$CFG->prefix}groups g WHERE  g.id > 0 $wsql";
        $r = $DB->get_record_sql($sql);
        $r =$r->countrecord;
        return $r;
   } 
   
    public function search($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
      
        $param=$util->addDefault($param,'limit',10);
        $param=$util->addDefault($param,'offset',0);
        
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
     
        $wsql=$this->search_wsql($param);
        $sql="SELECT g.id,g.name  FROM {$CFG->prefix}groups g WHERE  g.id > 0 $wsql ORDER BY g.name DESC ";
       
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
 
}

?>
