<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
require_once("$CFG->dirroot/local/badiuws/lib/baselib.php");
class local_badiuws_lib_coursecategory extends local_badiuws_baselib{

function __construct() {
		parent::__construct();
	}
    function get_tree($id=null,$name=null) {
        global $CFG;
		$displaylist=array();
		if($CFG->version < 2018120310){
			 require_once("$CFG->dirroot/lib/coursecatlib.php");
			$displaylist = coursecat::make_categories_list();
		}else{
			 $displaylist = core_course_category::make_categories_list();
		}
			
      
        $list=$this->get_children($id,$name);
        if (empty($id) && empty($name)){ return $displaylist;}
        $newdisplaylist=array();
            foreach ($list as $l) {
               
                 $id=$l->id;
                if (array_key_exists($id,$displaylist)){
                    $nametree=$displaylist[$id];
                    $newdisplaylist[$id]=$nametree;
                }
                
            } 
          return $newdisplaylist;
        
    }
  function get_parents($id) {
        global $CFG;
		$listparent=null;
		if($CFG->version < 2018120310){
			require_once("$CFG->dirroot/lib/coursecatlib.php");
			 $listparent=coursecat::get($id)->get_parents();
		}else{
			$listparent=core_course_category::get($id)->get_parents(); 
		}
		return $listparent;
     }
     
    function get_path($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  path FROM {$CFG->prefix}course_categories WHERE id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r->path;
    }

    function get_children($id,$name) {
        global $DB;
        global $CFG;
      
        if (empty($id) && empty($name)){return null;}
        $wsql="";
        $cont=0;
        if($id > 0){
             $path = $this->get_path($id);
             $wsql=" AND path LIKE '".$path."/%' OR id = $id ";
             $cont++;
        }
        if(!empty($name)){
           $wsql.=" AND LOWER(CONCAT(name,id))  LIKE '%".strtolower($name)."%'";
            $cont++;
        }
        if ($cont==0){return null;}
        $sql = "SELECT  id,name,path FROM {$CFG->prefix}course_categories WHERE id > 0 $wsql  ";
        
         $r = $DB->get_records_sql($sql);
         return $r;
    }

   function get_name($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  name FROM {$CFG->prefix}course_categories WHERE id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r->name;
    }
	
	function update($param) {
			$id=$this->getUtildata()->getVaueOfArray($param,'id');
			$name=$this->getUtildata()->getVaueOfArray($param,'name');
			if(empty($id)){return null;}
			$aparam=new stdClass();
			$aparam->id=$id;
			$aparam->name=$name;
		    $aparam->timemodified=time();
			global $DB;
			$result=$DB->update_record('course_categories', $aparam);
			return $result;
    }
}

?>
