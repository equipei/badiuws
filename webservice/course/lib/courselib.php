<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
require_once("$CFG->dirroot/local/badiuws/lib/baselib.php");

class local_badiuws_lib_course extends local_badiuws_baselib{
function __construct() {
		parent::__construct();
	}
   function get_list($name,$categoryid=null,$offset,$limit) {
         global $DB;
        global $CFG;
        $wsql="";
          if(!empty($categoryid)){
          $wsql=" AND category= $categoryid ";
        }
        if(!empty($name)){
            $wsql.=" AND LOWER(CONCAT(id,fullname,shortname))  LIKE '%".strtolower($name)."%'";
        }
        $sql = "SELECT  id,fullname  AS name FROM {$CFG->prefix}course  WHERE  id > 1 $wsql";
     
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
    }
    
   function get_name($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  fullname AS name FROM {$CFG->prefix}course WHERE id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r->name;
    }
    
    function get_info($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  c.fullname AS name,c.shortname,ct.name AS category,ct.id AS  categoryid FROM {$CFG->prefix}course c  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE c.id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r;
    }
    
     function create($param) {
         $course['fullname'] = $param['fullname'];
         $course['shortname'] =$param['shortname'];
         $course['categoryid'] =$param['categoryid'];
       
         $courses = array($course);
         $result = core_course_external::create_courses($courses);
         $result=$result[0]['id'];
        return $result;
    }
	function update($param) {
			$id=$this->getUtildata()->getVaueOfArray($param,'id');
			$fullname=$this->getUtildata()->getVaueOfArray($param,'fullname');
			if(empty($id)){return null;}
			$aparam=new stdClass();
			$aparam->id=$id;
			$aparam->fullname=$fullname;
		    $aparam->timemodified=time();
			global $DB;
			$result=$DB->update_record('course', $aparam);
			return $result;
    }
}

?>
