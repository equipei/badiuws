<?php

require_once ($CFG->libdir . '/filelib.php');
require_once ($CFG->libdir . '/completionlib.php');
require_once ($CFG->libdir . '/modinfolib.php');
require_once("$CFG->dirroot/course/renderer.php");

require_once($CFG->dirroot.'/course/format/renderer.php');
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
class local_badiuws_lib_coursecontent {

    function get_full_by_course($courseid,$coursetopic=null) {

        global $DB;
        global $PAGE;
        $list = array();
        $course = $DB->get_record('course', array('id' => $courseid));
        $renderer = new core_course_renderer($PAGE, null);
        $modinfo = get_fast_modinfo($course);
        $displayoptions = array();
        $sectionreturn = null;

   
        $list['topicos'] = $modinfo->sections;
 
       $completioninfo = new completion_info($course);

        //get atividadeshtmliconedit
        $atividadeshtmliconedit = array();
        $ativitieshtml = array();
        $addcontroll = array();
        global $USER;
        //$USER->editing = 1;
        foreach ($list['topicos'] as $k => $v) {
            if($coursetopic >=0 && $k ==$coursetopic){
               
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities

                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    //$controll=  $renderer->course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array());

                    $ativitieshtml[$a] = $activityhml;
                    //get edit eicon
                    $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                    $editicone = $renderer->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                    $atividadeshtmliconedit[$a] = $editicone;
                }
            }else  if($coursetopic===null){
               
                 foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities

                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    //$controll=  $renderer->course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array());

                    $ativitieshtml[$a] = $activityhml;
                    //get edit eicon
                    $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                    $editicone = $renderer->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                    $atividadeshtmliconedit[$a] = $editicone;
                }
            }
            
            $addcontroll[] = $renderer->course_section_add_cm_control($course, 0);


            $list['atividadeshtmliconedit'] = $atividadeshtmliconedit;
            $list['ativitieshtml'] = $ativitieshtml;
            $list['addcontroll'] = $addcontroll;
        }

        return $list;
    }   

 
    function get_list_item($courseid,$coursetopic=null) {

        global $DB;
        global $PAGE;
        $list = array();
        $course = $DB->get_record('course', array('id' => $courseid));
        $renderer = new core_course_renderer($PAGE, null);
        $modinfo = get_fast_modinfo($course);
        $displayoptions = array();
        $sectionreturn = null;


        $list['topicos'] = $modinfo->sections;

        $completioninfo = new completion_info($course);

       
        $ativitieshtml = array();
    
        global $USER;
        $USER->editing = null;
        foreach ($list['topicos'] as $k => $v) {
            if($coursetopic >=0 && $k ==$coursetopic){
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities
                    //code of course_section_cm_list_item in MOODLE_DIR_INSTALL/renderer.php
                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
                    $output = html_writer::tag('li', $activityhml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
                    $ativitieshtml[$a] = $activityhml;
              }

            }else  if($coursetopic===null){
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities
                    //code of course_section_cm_list_item in MOODLE_DIR_INSTALL/renderer.php
                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
                    $output = html_writer::tag('li', $activityhml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
                    $ativitieshtml[$a] = $activityhml;
              }
            }
            
          
        }

        return $ativitieshtml;
    }      
 function get_list($courseid,$section=null) {
         if(empty($courseid)){return null;}
        global $DB;
        global $CFG;
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
        $sql = "SELECT cm.id,m.name AS module,cm.instance,cm.idnumber,s.id AS topicid,s.section,s.sequence AS activitysequence,cm.visible,cm.visible,cm.visibleold,cm.groupmode,cm.completion,cm.completiongradeitemnumber,cm.completionview,cm.added FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section WHERE cm.deletioninprogress=0 AND s.course=cm.course AND cm.course=$courseid $wsql ORDER BY s.section ";
        $listcontent = $DB->get_records_sql($sql); 
        
        $listcontent=$this->add_info_tolist($listcontent,$courseid,$section);
        $listcontent=$this->oder_list($listcontent);
        return $listcontent;
    }
    
   private  function add_info_tolist($listcontent,$courseid,$section=null) {
       if(empty($listcontent)) {return null;}
        $listcontentnew=array();
        $listplugins=array();
        foreach ($listcontent as $d) {
               $name=$d->module;
              $listplugins[$name]=  $name;
         }
       $listcontentname=array();
       foreach ($listplugins as $plugin) {
           $instacelist=$this->get_name($plugin,$courseid);
           foreach ($instacelist as $linfo) {
               $id=$linfo->id;
               $key="$plugin/$id";
               $listcontentname[$key]=$linfo;
            }
           
       }
       foreach ( $listcontent as $lcontent) {
           $instance=$lcontent->instance;
           $module=$lcontent->module;
           $key="$module/$instance";
           if(array_key_exists($key,$listcontentname)){
               $info=$listcontentname[$key];
               $lcontent->name=$info->name;
               $lcontent->intro=$info->intro;
           }
          array_push($listcontentnew,$lcontent); 
       }
     return $listcontentnew;
    }
    
   private  function oder_list($listcontent) {
          if(empty($listcontent)) {return null;}
          $newlistcontent=array();
          $listbykey=array();
          $sectionsequences=array();
          foreach ($listcontent as $lc) {
             $key= $lc->section."/".$lc->id;
             $listbykey[$key]=$lc;
             
             $seq=array();
             $pos=stripos($lc->activitysequence, ",");
              if($pos=== false){
                 $seq=array($lc->activitysequence); 
              }else{
                  $seq=explode(",",$lc->activitysequence);
              }
             $sectionsequences[$lc->section]=$seq;
          }
          foreach ( $sectionsequences as $skey => $listidativity) {
              foreach ($listidativity as $ativityid) {
                  $ativkey=$skey."/".$ativityid;
                  if (array_key_exists($ativkey,$listbykey)){
                      $itemativity=$listbykey[$ativkey];
                     array_push($newlistcontent,$itemativity);  
                  }
              }
            }
        return   $newlistcontent;    
    }
    
  private function get_name($plugin,$courseid) {
        global $DB;
        global $CFG;
        $sql="SELECT id,name,intro FROM {$CFG->prefix}$plugin WHERE course=$courseid";
        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    
    function get_plugins() {
        global $DB;
        global $CFG;
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
        $sql = "SELECT cm.id,m.name,cm.instance,cm.idnumber,s.id AS topicid,s.section,cm.visible,cm.visible,cm.visibleold,cm.groupmode,cm.completion,cm.completiongradeitemnumber,cm.completionview,cm.added FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section WHERE  s.course=cm.course AND cm.course=$courseid $wsql ";

       
        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    
     function get_topics($param) {
         $util=new local_badiuws_util();
        global $DB;
        global $CFG;
        $courseid=$util->getVaueOfArray($param,'courseid');
        $visible=$util->getVaueOfArray($param,'visible');
        if(empty($visible)){$visible=1;}
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
        $sql = "SELECT id,name,section FROM {$CFG->prefix}course_sections WHERE course=$courseid AND visible=$visible ORDER BY section ";

        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    
    function get_topic_info_by_section($courseid,$section) {
       
        global $DB;
        global $CFG;
        
        $sql = "SELECT s.id,s.name,s.section,c.fullname AS course,c.shortname AS courseshortname,ct.name AS category,ct.id AS  categoryid  FROM {$CFG->prefix}course_sections s INNER JOIN {$CFG->prefix}course c   ON c.id=s.course INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE s.course=$courseid AND s.section= $section";

        $row = $DB->get_record_sql($sql);
        return $row;
    }
}

?>
