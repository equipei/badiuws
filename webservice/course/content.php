
<?php 
require_once("$CFG->dirroot/course/externallib.php");
require_once("$CFG->dirroot/local/badiuws/webservice/course/lib/contentlib.php");
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");

class local_badiuws_coursecontent  extends local_badiuws_baserole  {

    private $lib;
  function __construct() {
       parent::__construct();
       $this->lib=new local_badiuws_lib_coursecontent();
    }
    
     public function getfullbycourse() {
       $result=array();
       $courseid=null;
     
       global $DB;
            if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
          
       try {
            
           $result=$this->lib->get_full_by_course($courseid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 public function getlistitembycourse() {
       $result=array();
       $courseid=null;
     
       global $DB;
            if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
          
       try {
            
           $result=$this->lib->get_list_item($courseid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
public function getlistbycourse() {
       $result=array();
       $courseid=null;
       $section=null;
       global $DB;
            if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];

            if(isset($this->getParam()['section'])){
                if(!is_int((int)$this->getParam()['section'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.isnotnumber');}
                 $section=$this->getParam()['section'];
           }
          
          
       try {
            
           $result=$this->lib->get_list_by_course($courseid,$section);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}
$badiuwsdata=new local_badiuws_coursecontent();


?>
