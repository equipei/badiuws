
<?php

require_once("$CFG->dirroot/course/externallib.php");
require_once("$CFG->dirroot/local/badiuws/webservice/course/lib/courselib.php");
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");

class local_badiuws_course extends local_badiuws_baserole {

    private $lib;

    function __construct() {
        parent::__construct();
        $this->lib = new local_badiuws_lib_course();
    }

    public function create() {

        global $DB;
        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }



        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/user:create', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/user:create');
        }


        $cparam['fullname'] = $this->getParam()['name'];
        $cparam['shortname'] = $this->getParam()['shortname'];
        $cparam['categoryid'] = $this->getParam()['categoryid'];

        $result = null;
        try {
            $result = $this->lib->create($cparam);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }

        return $result;
    }
 public function update() {
        global $DB;
		$id=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		$name=$this->getUtildata()->getVaueOfArray($this->getParam(),'name');
		
		if(empty($id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isrequired');}
		if(!is_int((int)$id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
		
        if(empty($name)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
        
		$aparam=array();
		$aparam['id']=$id;
		$aparam['fullname']=$name;
		$result=null;
        try {
              $result=$this->lib->update($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
    public function createloop() {
        
        global $DB;
        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }

        if (!isset($this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');
        }
        if (!is_int((int) $this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');
        }


        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/user:create', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/user:create');
        }
          $cont = 0;
          $process = 0;
          $limit = $this->getParam()['limit'];
        for ($index = 0; $index < $limit; $index++) {
              $cont++;
              $cparam=array();
             $cparam['fullname'] = $this->getParam()['name'].' - '.$cont;
             $cparam['shortname'] = $this->getParam()['shortname'].'-'.$cont;
             $cparam['categoryid'] = $this->getParam()['categoryid'];
             
             try {
                   $result = $this->lib->create($cparam);
                    if ($result > 0) {$process++;}
             } catch (Exception $ex) {
                    $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
             }
        }
       
        return $process;
    }

    public function getlist() {
        $name = null;
        $categoryid = null;
        if (isset($this->getParam()['name'])) {
            $name = $this->getParam()['name'];
        }
        if (isset($this->getParam()['categoryid'])) {
            if (!is_int((int) $this->getParam()['categoryid'])) {
                $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
            }
            $categoryid = $this->getParam()['categoryid'];
        }
        $offset = $this->getPaginationOffset();
        $limit = $this->getPaginationLimit();
        $result = array();

        try {

            $list = $this->lib->get_list($name, $categoryid, $offset, $limit);

            foreach ($list as $value) {
                array_push($result, array('id' => $value->id, 'name' => $value->name));
            }
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        return $result;
    }

    public function getname() {
        $result = array();
        $id = null;

        global $DB;
        if (!isset($this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');
        }
        if (empty($this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');
        }
        if (!is_int((int) $this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['id']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['id'] . ' not exist in database in the table course');
        }
        $id = $this->getParam()['id'];

        try {

            $name = $this->lib->get_name($id);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        return $name;
    }

    public function duplicate() {
        $sourcecourseid = null;
        $name = null;
        $shortname = null;
        $categoryid = null;

        global $DB;
        if (!isset($this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.undefined');
        }
        if (!is_int((int) $this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['sourcecourseid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['sourcecourseid'] . ' not exist in database in the table course');
        }

        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }



        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:create');
        }

        $sourcecourseid = $this->getParam()['sourcecourseid'];
        $name = $this->getParam()['name'];
        $shortname = $this->getParam()['shortname'];
        $categoryid = $this->getParam()['categoryid'];

        $result = null;
        try {
            $cextlib = new core_course_external();
			 $options = array(

                array('name' => 'activities', 'value' => 1),

                array('name' => 'blocks', 'value' => 1),

                array('name' => 'filters', 'value' => 1),

                array('name' => 'users', 'value' => 0),

                array('name' => 'role_assignments', 'value' => 0),

                array('name' => 'comments', 'value' => 0),

                array('name' => 'logs', 'value' => 0),

            );
		$visible=1;
            $result = $cextlib->duplicate_course($sourcecourseid, $name, $shortname, $categoryid,$visible,$options);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        $result = $result['id'];
        return $result;
    }

    public function duplicateloop() {
        $sourcecourseid = null;
        $name = null;
        $shortname = null;
        $categoryid = null;
        $limit = null;
        global $DB;
        if (!isset($this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.undefined');
        }
        if (!is_int((int) $this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['sourcecourseid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['sourcecourseid'] . ' not exist in database in the table course');
        }

        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }

        if (!isset($this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');
        }
        if (!is_int((int) $this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');
        }


        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:create');
        }

        $sourcecourseid = $this->getParam()['sourcecourseid'];
        $name = $this->getParam()['name'];
        $shortname = $this->getParam()['shortname'];
        $categoryid = $this->getParam()['categoryid'];
        $limit = $this->getParam()['limit'];
        $result = null;
        $cont = 0;
        try {
            $cextlib = new core_course_external();
            for ($index = 0; $index < $limit; $index++) {
                $cont++;
                $result = $cextlib->duplicate_course($sourcecourseid, $name . ' - ' . $cont, $shortname . '-' . $cont, $categoryid);
            }
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }

        return $cont;
    }

}

$badiuwsdata = new local_badiuws_course();
//http://hotexamples.com/examples/-/core_course_external/create_courses/php-core_course_external-create_courses-method-examples.html<?php 
?>
