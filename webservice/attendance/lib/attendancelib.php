<?php

class local_badiuws_lib_attendance {

  public function get_list_by_couseid($courseid,$offset=0,$limit=10) {
        global $DB, $CFG;
        $sql ="SELECT l.id,u.id AS userid,u.firstname,u.lastname,u.email,s.sessdate,st.description,st.grade FROM {$CFG->prefix}attendance_log l INNER JOIN {$CFG->prefix}attendance_sessions s ON l.sessionid=s.id INNER JOIN {$CFG->prefix}attendance_statuses st ON l.statusid=st.id INNER JOIN {$CFG->prefix}user u ON l.studentid =u.id INNER JOIN {$CFG->prefix}attendance at ON s.attendanceid=at.id WHERE at.course =$courseid";
        $rows=$DB->get_records_sql($sql,null,$offset,$limit);
        return $rows;
      
    }
    public function get_list_by_userid($courseid,$userid,$offset=0,$limit=10) {
        global $DB, $CFG;
        $sql ="SELECT l.id,u.id AS userid,u.firstname,u.lastname,u.email,s.sessdate,st.description,st.grade FROM {$CFG->prefix}attendance_log l INNER JOIN {$CFG->prefix}attendance_sessions s ON l.sessionid=s.id INNER JOIN {$CFG->prefix}attendance_statuses st ON l.statusid=st.id INNER JOIN {$CFG->prefix}user u ON l.studentid =u.id INNER JOIN {$CFG->prefix}attendance at ON s.attendanceid=at.id WHERE at.course =$courseid AND u.id=$userid ";
        $rows=$DB->get_records_sql($sql,null,$offset,$limit);
        return $rows;
      
    }
    public function get_last_date_user_presence($courseid,$userid) {
        global $DB, $CFG;
        $sql ="SELECT MAX(s.sessdate) AS sessdate FROM {$CFG->prefix}attendance_log l INNER JOIN {$CFG->prefix}attendance_sessions s ON l.sessionid=s.id INNER JOIN {$CFG->prefix}attendance_statuses st ON l.statusid=st.id INNER JOIN {$CFG->prefix}user u ON l.studentid =u.id INNER JOIN {$CFG->prefix}attendance at ON s.attendanceid=at.id WHERE at.course =$courseid AND u.id=$userid  AND st.grade >  0 ";
        $row=$DB->get_record_sql($sql);
        return $row->sessdate;
      
    }
}

?>
