<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/attendance/lib/attendancelib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_attendance extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_attendance();
   }
   
   public function getlistbycouseid() {
        $result=array();
        $courseid=null;
        $offset=0;
        $limit=10;
        
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
      
           if(!isset($this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.undefined');}
           if(!is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
           else{ $offset=$this->getParam()['offset'];}
         
            if(!isset($this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');}
           if(!is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
           else{ $offset=$this->getParam()['limit'];}
           
       try {
            
           $result=$this->lib->get_list_by_couseid($courseid,$offset,$limit);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 public function getlistbyuserid() {
        $result=array();
        $courseid=null;
        $userid=null;
        $offset=0;
        $limit=10;
        
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
      
            if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
            if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
           if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
           if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
           $userid=$this->getParam()['userid'];
      
           if(!isset($this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.undefined');}
           if(!is_int((int)$this->getParam()['offset'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.offset.isnotnumber');}
           else{ $offset=$this->getParam()['offset'];}
         
            if(!isset($this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');}
           if(!is_int((int)$this->getParam()['limit'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');}
           else{ $offset=$this->getParam()['limit'];}
           
       try {
            
           $result=$this->lib->get_list_by_userid($courseid,$userid,$offset,$limit);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   public function getlastdateuserpresence() {
        $result=array();
        $courseid=null;
        $userid=null;
       
        
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
      
            if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
            if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
           if(!is_int((int)$this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.isnotnumber');}
           if(!$DB->record_exists('user', array('id' => $this->getParam()['userid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
           $userid=$this->getParam()['userid'];
      
          
           
       try {
            
           $result=$this->lib->get_last_date_user_presence($courseid,$userid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}

$badiuwsdata=new local_badiuws_webservice_attendance();
?>
