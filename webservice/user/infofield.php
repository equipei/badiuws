<?php
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/field.php");
 class local_badiuws_webservice_user_info_field extends local_badiuws_baserole  {
       private $lib;

    function __construct() {
          parent::__construct();
       
         $this->lib=new local_badiuws_lib_user_info_field();
    }
      public function create() {
          global $DB;
            if(!isset($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');}
            if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
            
            if(!isset($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');}
            if(empty($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');}
            
            if(!isset($this->getParam()['datatype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.datatype.undefined');}
            if(empty($this->getParam()['datatype'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.datatype.empty');}
           
             
            if(!isset($this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');}
            if(empty($this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.empty');}
            if(!is_int((int)$this->getParam()['categoryid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');}
            if(!$DB->record_exists('user_info_category', array('id' => $this->getParam()['categoryid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.notexist');}
           
             
            $result=null; 
            try {
                $dto=$this->lib->get_default_dto($this->getParam());
                $result=$this->lib->save($dto);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
          public function existbyshortname() {
            if(!isset($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');}
            if(empty($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');}
            $shortname=$this->getParam()['shortname'];
           
            
            $result=null; 
            try {
                 $result=$this->lib->exist_by_shortname($shortname);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
      public function getidbyshortname() {
             if(!isset($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamee.undefined');}
            if(empty($this->getParam()['shortname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');}
            $shortname=$this->getParam()['shortname'];
            $result=null; 
            try {
               $result=$this->lib->get_id_by_shortname($shortname);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
    public function existbyid() {
             if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
            if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
           
            $result=null; 
            try {
               $result=$this->lib->exist_by_id($this->getParam()['id']);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
 }
 
  $badiuwsdata=new local_badiuws_webservice_user_info_field();
  
?>