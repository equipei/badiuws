
<?php 

require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/userlib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_user extends local_badiuws_baserole  {
    
    private $lib;

    function __construct() {
          parent::__construct();
       
         $this->lib=new local_badiuws_lib_user();
    }
    
    //native api moodle
    public function create() {
        global $DB;
       global $CFG;
         if(!isset($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.undefined');}
         if(empty($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
         
           
          if(!isset($this->getParam()['firstname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.firstname.undefined');}
         if(empty($this->getParam()['firstname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.firstname.empty');}
       
           
          if(!isset($this->getParam()['lastname'])){ $this->getResponse()->danied('badiu.moodle.ws.param.lastname.undefined');}
         if(empty($this->getParam()['lastname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.lastname.empty');}
       
         
          if(!isset($this->getParam()['email'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.undefined');}
         if(empty($this->getParam()['email'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.empty');}
         if(!filter_var($this->getParam()['email'],FILTER_VALIDATE_EMAIL)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.notvalid');}
         
         if(!isset($this->getParam()['password'])){ $this->getResponse()->danied('badiu.moodle.ws.param.password.undefined');}
         if(empty($this->getParam()['password'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.password.empty');}
        
       //check permission
         $this->login();
        $permission=has_capability('moodle/user:create',context_system::instance());
        if(!$permission){ $this->getResponse()->danied('badiu.moodle.ws.error.nopermission','moodle/user:create');}
        
        //check exist login
        $existlogin=$DB->record_exists('user', array('username' => $this->getParam()['username'], 'mnethostid' => $CFG->mnet_localhost_id));
    
        if ($existlogin) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateusername','username '. $this->getParam()['username'].' just exist in database');
            }
        
        //exist email
         $existemail=$DB->record_exists('user', array('email' => $this->getParam()['email'], 'mnethostid' => $CFG->mnet_localhost_id));
         if ($existemail) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateemail','E-mail '. $this->getParam()['email'].' just exist in database');
            }
            
       
        $users = array($this->getParam());
        $result=null; 
        try {
             $result = core_user_external::create_users($users);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
        
     
         $id=$result[0]['id'];
        return $id;
    }
  
    public function add() {
        global $DB;
       global $CFG;
       
         if(!isset($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.undefined');}
         if(empty($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
         
           
          if(!isset($this->getParam()['firstname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.firstname.undefined');}
         if(empty($this->getParam()['firstname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.firstname.empty');}
       
           
          if(!isset($this->getParam()['lastname'])){ $this->getResponse()->danied('badiu.moodle.ws.param.lastname.undefined');}
         if(empty($this->getParam()['lastname'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.lastname.empty');}
       
         
          if(!isset($this->getParam()['email'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.undefined');}
         if(empty($this->getParam()['email'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.empty');}
         if(!filter_var($this->getParam()['email'], FILTER_VALIDATE_EMAIL)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.email.notvalid');}
         
         if(!isset($this->getParam()['password'])){ $this->getResponse()->danied('badiu.moodle.ws.param.password.undefined');}
         if(empty($this->getParam()['password'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.password.empty');}
        
        
        //check exist login
        $existlogin=$DB->record_exists('user', array('username' => $this->getParam()['username'], 'mnethostid' => $CFG->mnet_localhost_id));
    
        if ($existlogin) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateusername','username '. $this->getParam()['username'].' just exist in database');
            }
        
        //exist email
         $existemail=$DB->record_exists('user', array('email' => $this->getParam()['email'], 'mnethostid' => $CFG->mnet_localhost_id));
         if ($existemail) {
                $this->getResponse()->danied('badiu.moodle.ws.error.dulicateemail','E-mail '. $this->getParam()['email'].' just exist in database');
            }
            
       
       
        $result=null; 
        try {
             $result = $this->lib->add($this->getParam());
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
        
        return $result;
    }
	  public function update() {
		    global $DB;
			global $CFG;
		  $id=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		  $username=$this->getUtildata()->getVaueOfArray($this->getParam(),'username');
		  $email=$this->getUtildata()->getVaueOfArray($this->getParam(),'email');
		  $idnumber=$this->getUtildata()->getVaueOfArray($this->getParam(),'idnumber');
		
		   if(empty($id)){$this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
		   $existid=$DB->record_exists('user', array('id' => $id));
		   if (!$existid) {$this->getResponse()->danied('badiu.moodle.ws.error.useridnotexist','id '. $id.' not exist in database');}
		   
		   if(!empty($username)){
				$existusername=$this->lib->exist_username_edit($id,$username);
				if ($existusername) {$this->getResponse()->danied('badiu.moodle.ws.error.usernameexistregistredotheruser','username '. $username.' just exist in database registred in other user');}
		   }
		   if(!empty($email)){
				$existemail=$this->lib->exist_email_edit($id,$email);
				if ($existemail) {$this->getResponse()->danied('badiu.moodle.ws.error.emailexistregistredotheruser','email '. $email.' just exist in database registred in other user');}
		   }
		  
			if(!empty($idnumber)){		  
				$existidnumber=$this->lib->exist_idnumber_edit($id,$idnumber);
				if ($existidnumber) {$this->getResponse()->danied('badiu.moodle.ws.error.idnumberexistregistredotheruser','idnumber '. $username.' just exist in database registred in other user');}
		    }
			
		  $result=null; 
          
            try {
              $result= $this->lib->update($this->getParam());
              } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
		  
	  }
     public function getidbyusername() {
            if(!isset($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.undefined');}
            if(empty($this->getParam()['username'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
            $username=$this->getParam()['username'];
            
            $result=null; 
             global $DB, $CFG;
            try {
              
              $result= $this->lib->get_id_by_username($username);
               if(empty($result)){$result="";}
               else {$result=$result->id;}
            } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
   public function getinfo() {
           global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
            if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
             if(!$DB->record_exists('user', array('id' => $this->getParam()['id']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['id'].' not exist in database in the table user');}
            $id=$this->getParam()['id'];
            
            $result=null; 
            
            try {
              
              $result= $this->lib->get_info($id);
               if(empty($result)){$result="";}
               else {$result=(array)$result;}
            } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    public function getinfodetail() {
            global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
            if(!$DB->record_exists('user', array('id' => $this->getParam()['id']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['id'].' not exist in database in the table user');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.username.empty');}
            $id=$this->getParam()['id'];
            
            $result=null; 
            
            try {
              
              $result= $this->lib->get_info_detail($id);
               if(empty($result)){$result="";}
               else {$result=(array)$result;}
            } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
}
$badiuwsdata=new local_badiuws_webservice_user();


?>
