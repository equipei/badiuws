<?php
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/category.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/field.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/data.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/datareport.php");
 class local_badiuws_webservice_user_info_data extends local_badiuws_baserole  {
       private $lib;

    function __construct() {
          parent::__construct();
       
         $this->lib=new local_badiuws_lib_user_info_field_data();
    }
      public function profile() {
          global $DB;
            if(!isset($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.undefined');}
            if(empty($this->getParam()['userid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.userid.empty');}
            $exist=$DB->record_exists('user', array('id' => $this->getParam()['userid']));
            if(!$exist){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['userid'].' not exist in database in the table user');}
            $userid=$this->getParam()['userid'];
            $result=null; 
            try {
                $libcat=new local_badiuws_lib_user_info_field_category();
                $libfield=new local_badiuws_lib_user_info_field();
               
                //lista category
                $categories=$libcat->get_all();
                $fields=$libfield->get_all();
                $datas=$this->lib->get_all_by_userid($userid);
                
                
                $datasf=array();
                foreach ($datas as $d) {
                   $datasf[$d->fieldid]=$d;
                }
              
                $result=array();
                foreach ($categories as $cat) {
                   $listud=array();
                   $listud['id']=$cat->id;
                   $listud['name']=$cat->name;
                   $listud['fields']=array();
                   foreach ($fields as $f) {
                        $field=(array)$f;
                        $field['userdata']=null;
                        if(isset($datasf[$f->id])){
                            $fdata=(array)$datasf[$f->id];
                            unset($fdata['fieldid']);
                            $field['userdata']=$fdata;
                       }
                        if($f->categoryid==$cat->id){array_push($listud['fields'],$field);}
                    }
                    array_push($result,$listud);
                }
           
               
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
         
          return $result;
    }
    
     public function report() {
          global $DB;
          $param=$this->getParam();
             $result=null; 
            try {
                $libreport=new local_badiuws_lib_user_info_field_datareport();
               $result=$libreport->makeReport($param);
               } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
           echo "<pre>";
          print_r($result);
          echo "</pre>";exit; 
          return $result;
    }
 }
 
 $badiuwsdata=new local_badiuws_webservice_user_info_data();
?>