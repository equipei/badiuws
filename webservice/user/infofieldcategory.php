<?php
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/user/lib/field/category.php");
 class local_badiuws_webservice_user_info_field_category extends local_badiuws_baserole  {
       private $lib;

    function __construct() {
          parent::__construct();
       
         $this->lib=new local_badiuws_lib_user_info_field_category();
    }
      public function create() {
            if(!isset($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');}
            if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
            $name=$this->getParam()['name'];
            if($this->lib->exist_by_name($name)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.duplicationname',$name.' just exist in database');}
            
            
            $result=null; 
            try {
               $result=$this->lib->save($name);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
          public function existbyname() {
            if(!isset($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');}
            if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
            $name=$this->getParam()['name'];
           
            
            $result=null; 
            try {
                 $result=$this->lib->exist_by_name($name);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
      public function getidbyname() {
             if(!isset($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');}
            if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
            $name=$this->getParam()['name'];
            $result=null; 
            try {
               $result=$this->lib->get_id_by_name($name);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
    
    public function existbyid() {
             if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
            if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
           
            $result=null; 
            try {
               $result=$this->lib->exist_by_id($this->getParam()['id']);
             } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
            }
          return $result;
    }
 }
 
  $badiuwsdata=new local_badiuws_webservice_user_info_field_category();
  
?>