
<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
class local_badiuws_lib_user {

    public function add($param) {
         $util=new local_badiuws_util();
         global $DB;
         $param=$util->addDefault($param,'timecreated',time());
         $param=$util->addDefault($param,'confirmed',1);
         $param=$util->addDefault($param,'mnethostid',1);
         $auth=$util->getVaueOfArray($param,'auth');
         $param['password']=hash_internal_user_password($param['password']);
         
         
         $dto=  (object)$param;
         return $DB->insert_record('user', $dto);
    }
    
    function get_info($id) {
        global $DB, $CFG;
        $sql = "SELECT id,email,firstname,lastname,username FROM {$CFG->prefix}user WHERE id = $id";
        $result = $DB->get_record_sql($sql);
        return $result;
    }

    function get_info_detail($id) {
        global $DB, $CFG;
        $sql = "SELECT id,email,firstname,lastname,username,idnumber,phone1,phone2,institution,department,address,city,lastnamephonetic,firstnamephonetic,alternatename FROM {$CFG->prefix}user WHERE id = $id";
        $result = $DB->get_record_sql($sql);
        return $result;
    }

    function get_id_by_username($username) {
        global $DB, $CFG;
        $sql = "SELECT id FROM {$CFG->prefix}user  WHERE username='" . $username . "'";
        $result = $DB->get_record_sql($sql);
        return $result;
    }
   function get_id_by_email($email) {
        global $DB, $CFG;
        $sql = "SELECT id FROM {$CFG->prefix}user  WHERE email='" . $email. "'";
        $result = $DB->get_record_sql($sql);
        if(empty($result->id)){return null;}
        return $result->id;
    }
    function get_piture_url($userid) {
         global $PAGE;
         $user = new stdClass();
         $user->id = $userid;

        $picture = new user_picture($user);
        $src = $picture->get_url($PAGE);
        $src = str_replace('f2', 'f3', $src);
        return $src;
    }

    function validate_email_autocomplete($str) { 
         if(empty($str)) return false;
         $str=trim($str);
        $p=explode(" - ",$str);
        $email=null;
        if(isset($p[1])){$email=$p[1];}
        if(!empty($email)){
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $id=$this->get_id_by_email($email);
                return $id;
            }
        }
        return false;
    }
    
     private function recentaccess_search_wsql($param) {
          $util=new local_badiuws_util();
          $wsql="";
           
          $exceptuserid=$util->getVaueOfArray($param,'exceptuserid');
          if(!empty($exceptuserid)){$wsql=" AND u.id !=$exceptuserid ";}
          return  $wsql;
     }
     public function recentaccess_search_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        $param=$util->addDefault($param,'recentaccess',time()-600);
        $recentaccess=$util->getVaueOfArray($param,'recentaccess');
        
        $wsql=$this->recentaccess_search_wsql($param);
        $sql="SELECT COUNT(DISTINCT u.id) AS countrecord  FROM {$CFG->prefix}logstore_standard_log l  INNER JOIN  {$CFG->prefix}user u ON l.userid=u.id WHERE  u.id > 0  AND l.timecreated >= $recentaccess  $wsql";
        $r = $DB->get_record_sql($sql);
        $r =$r->countrecord;
        return $r;
   } 
   
    public function recentaccess_search($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
      
        $param=$util->addDefault($param,'recentaccess',time()-300);
        $param=$util->addDefault($param,'limit',10);
        $param=$util->addDefault($param,'offset',0);
        
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        $recentaccess=$util->getVaueOfArray($param,'recentaccess');
        
        
        $wsql=$this->recentaccess_search_wsql($param);
        $sql="SELECT u.id,u.email,u.firstaccess,u.firstname,u.lastname,u.timecreated,MAX(l.timecreated) AS lastaccess,COUNT(l.id) AS countlog FROM {$CFG->prefix}logstore_standard_log l  INNER JOIN  {$CFG->prefix}user u ON l.userid=u.id WHERE u.id > 0  AND l.timecreated >= $recentaccess  $wsql    GROUP BY u.id,u.email,u.firstaccess,u.firstname,u.lastname,u.timecreated ORDER BY l.timecreated DESC ";
       
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
   
   
   private function offline_search_wsql($param) {
          $util=new local_badiuws_util();
          $wsql="";
           $exceptuserid=$util->getVaueOfArray($param,'exceptuserid');
          if(!empty($exceptuserid)){$wsql=" AND id !=$exceptuserid ";}          
          return  $wsql;
     }
     public function offline_search_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        $param=$util->addDefault($param,'lastaccess',time()-21600);
        $lastaccess=$util->getVaueOfArray($param,'lastaccess');
        
        $wsql=$this->offline_search_wsql($param);
        $sql="SELECT COUNT(id) AS countrecord  FROM {$CFG->prefix}user  WHERE id > 0 AND  (lastaccess < $lastaccess OR lastaccess IS NULL)  $wsql";
        $r = $DB->get_record_sql($sql);
        $r =$r->countrecord;
        return $r;
   } 
   
    public function offline_search($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
         $param=$util->addDefault($param,'lastaccess',time()-18000);
        $param=$util->addDefault($param,'limit',10);
        $param=$util->addDefault($param,'offset',0);
        
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
         $lastaccess=$util->getVaueOfArray($param,'lastaccess');
        
        
        $wsql=$this->offline_search_wsql($param);
        $sql="SELECT id,email,firstname,lastname,timecreated FROM {$CFG->prefix}user  WHERE id > 0 AND  (lastaccess < $lastaccess OR lastaccess IS NULL)  $wsql ORDER BY lastaccess DESC";
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
    
}

?>
