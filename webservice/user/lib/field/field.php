<?php
class  local_badiuws_lib_user_info_field {
    //required fields: shortname,name,categoryid,datatype
   function get_default_dto($param) {
       
            $dto = (object)$param;
               
            if(!isset($dto->description) && !$this->hasValue($dto->description)){$dto->description='';}
            if(!isset( $dto->descriptionformat) && $this->hasValue( $dto->descriptionformat)){ $dto->descriptionformat=1;}
            if(!isset($dto->sortorder) && !$this->hasValue($dto->sortorder)){$dto->sortorder=1;}
            if(!isset($dto->required) && !$this->hasValue($dto->required)){$dto->sortorder=0;}
            if(!isset($dto->locked) && !$this->hasValue($dto->locked)){$dto->locked=1;}
            if(!isset($dto->visible) && !$this->hasValue($dto->visible)){$dto->visible=2;}
            if(!isset($dto->forceunique) && !$this->hasValue($dto->forceunique)){$dto->forceunique=0;}
            if(!isset($dto->signup) && !$this->hasValue($dto->signup)){$dto->signup=0;}
            if(!isset($dto->defaultdata) && !$this->hasValue($dto->defaultdata)){$dto->defaultdata='';}
            if(!isset($dto->defaultdataformat) && !$this->hasValue($dto->defaultdataformat)){$dto->defaultdataformat=1;}
            if(!isset($dto->sortorder) && !$this->hasValue($dto->sortorder)){$dto->sortorder=1;}
            if(!isset($dto->sortorder) && !$this->hasValue($dto->sortorder)){$dto->sortorder=1;}
          
            if($dto->datatype=='text') {
                   if(!isset($dto->param1) && !$this->hasValue($dto->param1)){$dto->param1=60;}  
                   if(!isset($dto->param2) && !$this->hasValue($dto->param2)){$dto->param2=2048;}  
                   if(!isset($dto->param3) && !$this->hasValue($dto->param3)){$dto->param3=0;}  

            }else  if($dto->datatype=='datetime') {
                   if(!isset($dto->param1) && !$this->hasValue($dto->param1)){$dto->param1=1900;}  
                   if(!isset($dto->param2) && !$this->hasValue($dto->param2)){$dto->param2=2100;}  
                  

            }
            
           return $dto;
       }
      
        function hasValue($value) {
            if($value==0){return true;}
            if(empty($value)){return false;}
            return false;
        }
   
       
   function save($dto) {
            global $CFG,$DB;
            return $DB->insert_record('user_info_field', $dto);
       }

 function edit($dto) {
            global $CFG,$DB;
            return $DB->update_record('user_info_field', $dto);
       }

 
 

 function exist_by_shortname($shortname) {
    global $CFG,$DB;
     $sql ="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}user_info_field WHERE shortname='".$shortname."'";
     $r=$DB->get_record_sql($sql);
     return $r->qreg;
 }
  function exist_by_id($id) {
    global $CFG,$DB;
     $sql ="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}user_info_field WHERE id=$id";
     $r=$DB->get_record_sql($sql);
     return $r->qreg;
 }
  function get_id_by_shortname($shortname) {
    global $CFG,$DB;
     $sql ="SELECT id FROM {$CFG->prefix}user_info_field WHERE shortname='".$shortname."'";
     $r=$DB->get_record_sql($sql);
     return $r->id;
 }
 
 function get_by_categoryid($cotegoryid) {
    global $CFG,$DB;
     $sql ="SELECT id,shortname,name FROM {$CFG->prefix}user_info_field WHERE categoryid=$cotegoryid ORDER BY sortorder";
     $rows=$DB->get_records_sql($sql);
     return $rows;
 }
 
  function get_all() {
    global $CFG,$DB;
     $sql ="SELECT id,shortname,name,categoryid FROM {$CFG->prefix}user_info_field  ORDER BY categoryid,sortorder";
     $rows=$DB->get_records_sql($sql);
     return $rows;
 }
 function count() {
    global $CFG,$DB;
     $sql ="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}user_info_field ";
     $row=$DB->get_record_sql($sql);
     return $row->countrecord;
 }
}
?>