<?php
class  local_badiuws_lib_user_info_field_data {
  
    function get_fieldid_profile($fieldkey) {
         global $DB, $CFG;
         $sql="SELECT id FROM {$CFG->prefix}user_info_field WHERE shortname='".$fieldkey."'";
         $r= $DB->get_record_sql($sql);
        
         return $r->id;
    }
     
    function exist_data_profile($fieldid,$userid) {
         global $DB, $CFG;
         $r= $DB->get_record_sql("SELECT COUNT(id) AS qreg FROM {$CFG->prefix}user_info_data  WHERE fieldid=$fieldid AND userid= $userid ");
         return $r->qreg;
    }
     function get_id_data_profile($fieldid,$userid) {
         global $DB, $CFG;
         $r= $DB->get_record_sql("SELECT id FROM {$CFG->prefix}user_info_data  WHERE fieldid=$fieldid AND userid= $userid ");
         return $r->id;
    }
    function save_data_profile($fieldid,$userid,$data) {
         global $DB, $CFG;
         if(empty($data)){$data='';}
         $dto= new stdClass();
         $dto->fieldid=$fieldid;
         $dto->userid=$userid;
         $dto->data=$data;
        return  $DB->insert_record('user_info_data', $dto);
       
    }
    function update_data_profile($fieldid,$userid,$data) {
        global $DB, $CFG;
        if(empty($data)){$data='';}
         $dto= new stdClass();
         $dto->fieldid=$fieldid;
         $dto->userid=$userid;
         $dto->data=$data;
         $dto->id=$this->get_id_data_profile($fieldid,$userid) ;
         return $DB->update_record('user_info_data', $dto);
    }
    
    function add_data_profile($fieldkey,$userid,$data) {
        $data=addslashes($data);
       
        $result=null;
        $fieldid=$this->get_fieldid_profile($fieldkey);
      
        //verificar se já existe
        $exist=$this->exist_data_profile($fieldid,$userid);
        
       // echo "<br>exist $exist ";
        //gravar se não existe
        if(!$exist){
                 $result= $this->save_data_profile($fieldid,$userid,$data);
        }
        //atualiza se existir
        else{
             $result= $this->update_data_profile($fieldid,$userid,$data);
        }

        return $result;
      }  
      
      function get_data_by_profile($fieldkey,$userid) {
          global $DB, $CFG;
         $sql="SELECT d.data  FROM {$CFG->prefix}user_info_data d INNER JOIN {$CFG->prefix}user_info_field f ON d.fieldid=f.id WHERE f.shortname='".$fieldkey."' AND d.userid= $userid ";
        
         $r= $DB->get_record_sql($sql);
         return $r->data;
    }
    
       function get_datas_by_profile($userid) {
         global $DB, $CFG;
         $sql="SELECT d.id,d.fieldid,f.shortname,d.data  FROM {$CFG->prefix}user_info_data d INNER JOIN {$CFG->prefix}user_info_field f ON d.fieldid=f.id WHERE d.userid= $userid ";
         $rows= $DB->get_records_sql($sql);
         return $rows;
    }
    
     function get_all_by_userid($userid) {
         global $DB, $CFG;
         $sql="SELECT id,fieldid,data  FROM {$CFG->prefix}user_info_data WHERE userid= $userid ";
         $rows= $DB->get_records_sql($sql);
         return $rows;
    }
}
?>