<?php 
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
class  local_badiuws_lib_user_info_field_datareport {
    private $users;
    private $fields;
    private $data;
    private $util;
    private $fieldlib;
     private $dateformat='d/m/Y';
     function __construct() { 
      //  $this->withdatafilter=false;
           $this->util=new local_badiuws_util();
           $this->fieldlib=new local_badiuws_lib_user_info_field();
      }
   
      function makeReport($param=array()) {
          $this->users=$this->get_users($param);
          $this->data=$this->get_users_data($param);
        
         
          $this->fields=$this->fieldlib->get_all();
          
          $firtrow=$this->make_first_row($this->users,$this->fields);
         
          $result=array();
          array_push($result, $firtrow);
          $this->add_defultuserdata_to_data();
          foreach ($this->users as $user) {
              $userid=$user->id;
              $nrow=array();
              foreach ($firtrow  as $fkey => $fvalue) {
                    $dkey=$userid."/".$fkey;
                    $data=null;
                    if (array_key_exists($dkey,$this->data)){$data=$this->data[$dkey];}
                    $nrow[$fkey]=$data;
              } 
              array_push($result, $nrow);
          }
       
         return $result;
          
      }
      private function get_users_wsql($param) {
          $wsql="";
        
           
           $user=$this->util->getVaueOfArray($param,'user');
          if(!empty($user)){$wsql.=" AND LOWER(CONCAT(u.id,u.firstname,u.lastname,u.email,u.username,u.idnumber)) LIKE '%".strtolower($user)."%' ";}
         
          return $wsql;
          
          
     }
     private function get_users_course_wsql($param) {
          $wsql="";
          $course=$this->util->getVaueOfArray($param,'course');
          if(!empty($course)){$wsql.=" AND LOWER(CONCAT(c.id,c.fullname,c.shortname)) LIKE '%".strtolower($course)."%' ";}
          
         
          return $wsql;
          
          
     }
      function count_users($param=array()) {
         global $DB, $CFG;
         $courseid=$this->util->getVaueOfArray($param,'courseid');
         
         $sql=null;
         if($courseid){
             $wsql=$this->get_users_course_wsql($param);
             $sql="SELECT COUNT(DISTINCT u.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}user u ON u.id=rs.userid INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid LEFT JOIN  {$CFG->prefix}user_info_data d ON u.id=d.userid  WHERE  e.contextlevel=50 AND e.instanceid =$courseid  $wsql ";
          }
         else{
             $wsql=$this->get_users_wsql($param);
             $sql="SELECT COUNT(DISTINCT u.id) AS countrecord  FROM {$CFG->prefix}user u LEFT JOIN  {$CFG->prefix}user_info_data d ON u.id=d.userid WHERE u.id > 0 $wsql";
             
             }
         $result= $DB->get_record_sql($sql);
         $result=$result->countrecord;
         return  $result;
     }
     
       function get_users($param=array()) {
         global $DB, $CFG;
      
         $offset=0;
         $limit=10;
         if(isset($param['course'])){$course=$param['course'];}
         if(isset($param['offset'])){ $offset=$param['offset'];}
         if(isset($param['limit'])){ $limit=$param['limit'];}
         $offset=$offset*$limit;
          $courseid=$this->util->getVaueOfArray($param,'courseid');
         $sql=null;
          if($courseid){
               $wsql=$this->get_users_course_wsql($param);
              $sql="SELECT DISTINCT u.id,u.email,u.firstname,u.lastname,u.username FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}user u ON u.id=rs.userid INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid  LEFT JOIN  {$CFG->prefix}user_info_data d ON u.id=d.userid  WHERE  e.contextlevel=50 AND e.instanceid =$courseid  $wsql  ORDER BY u.firstname,u.lastname";
              
              }
         else{
              $wsql=$this->get_users_wsql($param);
             $sql="SELECT DISTINCT u.id,u.email,u.firstname,u.lastname,u.username FROM {$CFG->prefix}user u LEFT JOIN  {$CFG->prefix}user_info_data d ON u.id=d.userid WHERE u.id > 0 $wsql ORDER BY u.firstname,u.lastname";
         }
         $result= $DB->get_records_sql($sql,null,$offset,$limit);
       
         return  $result;
     }
     function get_users_data($param=array()) {
         global $DB, $CFG;
         
           $courseid=$this->util->getVaueOfArray($param,'courseid');
         $offset=0;
         $limit=10;
     
         if(isset($param['offset'])){ $offset=$param['offset'];}
         if(isset($param['limit'])){ $limit=$param['limit'];}
         
         $countfields=$this->fieldlib->count();
         $limit=$limit*$countfields;
         
        
         $offset=$offset*$limit;
         $sql=null;
         if( $courseid){
              $wsql=$this->get_users_course_wsql($param);
             $sql="SELECT d.id,d.fieldid,d.userid,d.data,f.shortname,f.datatype FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}user u ON u.id=rs.userid INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN mdl_course c ON c.id=e.instanceid  LEFT JOIN  {$CFG->prefix}user_info_data d  ON u.id=d.userid INNER JOIN {$CFG->prefix}user_info_field f ON d.fieldid=f.id  WHERE   e.contextlevel=50 AND e.instanceid =$courseid  $wsql  ";
         }
         else{
             $wsql=$this->get_users_wsql($param);
             $sql="SELECT d.id,d.fieldid,d.userid,d.data,f.shortname,f.datatype FROM {$CFG->prefix}user u LEFT JOIN  {$CFG->prefix}user_info_data d  ON u.id=d.userid INNER JOIN {$CFG->prefix}user_info_field f ON d.fieldid=f.id  WHERE u.id > 0  $wsql ";
         }
         $result = $DB->get_records_sql($sql,null,$offset,$limit);
         $dnew=array();
         foreach ($result as $row) {
             $key=$row->userid."/".$row->shortname;
             $data=$row->data;
             if($row->datatype=='datetime'){
                if(!empty($data)){$data= date($this->dateformat,$data);}
             }
             $dnew[$key]=$data;
         } 
        
         return $dnew;
     }
     
      function make_first_row($users,$fields) {
           //first rows with column
          $firstrow=array();
          $cont=0;
          foreach ($users as $user) {
              $user=(array)$user;
              foreach ($user as $key => $value) {
                  $firstrow[$key]=$key;
              }
              if($cont==0){break;}
              $cont++;
          }
         foreach ($fields as $field) {
             $key=$field->shortname;
             $name=$field->name;
             $firstrow[$key]=$name;
         }
         
         return $firstrow;
      }
     
    function add_defultuserdata_to_data() {
     foreach ($this->users as $user) {
               $user=(array)$user;
               $userid=$user['id'];
              foreach ($user as $key => $value) {
                  $key=$userid."/".$key ;
                  $this->data[$key]=$value;
              }
          }
    }
    
}

?>