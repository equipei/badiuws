<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/page/lib/pagelib.php");
require_once("$CFG->dirroot/user/externallib.php");
class local_badiuws_webservice_page extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_page();
   }
   
    public function getcontentbyid() {
       $result=array();
       $id=null;
     
       global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
          $id=$this->getParam()['id'];
          
       try {
            
           $result=$this->lib->get_content_by_id($id);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
  
  public function getcontentbycmid() {
       $result=array();
       $id=null;
     
       global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
          $id=$this->getParam()['id'];
          
       try {
            
           $result=$this->lib->get_content_by_cmid($id);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   public function getcontentsbycourseid() {
       $result=array();
        $courseid=null;
     
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
      
          
       try {
            
           $result=$this->lib->get_contents_by_courseid($courseid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }

    public function getcontentsbysection() {
       $result=array();
        $courseid=null;
        $section=null;
     
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
           if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}

           if(!isset($this->getParam()['section'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.undefined');}
           if($this->getParam()['section']==""){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.isnotnumber');}
         
           $courseid=$this->getParam()['courseid'];
           $section=$this->getParam()['section'];
      
          
       try {
            
           $result=$this->lib->get_contents_by_section($courseid,$section);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }

   public function getcontentsbyidnumber() {
       $result=array();
        $courseid=null;
        $section=null;
        $operator=null;
        $idnumber=null;
       global $DB;
             if(!isset($this->getParam()['operator'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.operator.undefined');}
            if(empty($this->getParam()['operator'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.operator.empty');}
            if($this->getParam()['operator']!='*' || $this->getParam()['operator']!='start' || $this->getParam()['operator']!='end' ){ $this->getResponse()->danied('badiu.moodle.ws.error.param.operator.notvalid','Value should be: * | start | end ');}
          
          if(!isset($this->getParam()['idnumber'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.idnumber.undefined');}
           if(empty($this->getParam()['idnumber'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.idnumber.empty');}
          

            if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
           if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}

           if(!isset($this->getParam()['section'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.undefined');}
           if($this->getParam()['section']==""){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.isnotnumber');}
         
            $operator=$this->getParam()['operator'];
           $courseid=$this->getParam()['courseid'];
           $section=$this->getParam()['section'];
           $idnumber=$this->getParam()['idnumber'];
      
          
       try {
            
           $result=$this->lib->et_contents_by_idnumber($operator,$idnumber,$courseid,$section);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   
    public function getcmidbycourseid() {
       $result=array();
        $courseid=null;
     
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
            if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}
           $courseid=$this->getParam()['courseid'];
      
          
       try {
            
           $result=$this->lib->get_cmid_by_courseid($courseid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
   
    public function getcmidbysection() {
       $result=array();
        $courseid=null;
        $section=null;
     
       global $DB;
           if(!isset($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');}
           if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
           if(!$DB->record_exists('course', array('id' => $this->getParam()['courseid']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist',$this->getParam()['courseid'].' not exist in database in the table course');}

           if(!isset($this->getParam()['section'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.undefined');}
           if($this->getParam()['section']==""){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.empty');}
           if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.section.isnotnumber');}
         
           $courseid=$this->getParam()['courseid'];
           $section=$this->getParam()['section'];
      
          
       try {
            
           $result=$this->lib->get_cmid_by_section($courseid,$section);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
    public function searchcount() {
           try {
            
           $result=$this->lib->search_count($this->getParam());
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
    public function search() {
           try {
            
           $result=$this->lib->search($this->getParam());
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}

$badiuwsdata=new local_badiuws_webservice_page();
?>
