<?php

class local_badiuws_lib_page {

  public function get_content_by_id($id) {
        global $DB, $CFG;
        $sql ="SELECT id,name,intro,content FROM {$CFG->prefix}page WHERE id=$id";
        $row=$DB->get_record_sql($sql); 
        return $row;
      
    }
  public function get_content_by_cmid($cmid) {
        global $DB, $CFG;
        $sql ="SELECT p.id,p.name,p.intro,p.content FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' AND cm.id=$cmid";
        $row=$DB->get_record_sql($sql); 
        return $row;
      
    }
 
   public function get_contents_by_courseid($courseid) {
        global $DB, $CFG;
         $sql ="SELECT id,name,intro,content FROM {$CFG->prefix}page WHERE course=$courseid";
        $rows=$DB->get_records_sql($sql); 
        return $rows;
    }
  public function get_contents_by_section($courseid,$section) {
        global $DB, $CFG;
         $sql ="SELECT p.id,p.name,p.intro,p.content FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' AND cm.course=$courseid AND s.section=$section";
        $rows=$DB->get_records_sql($sql); 
        return $rows;
    }
  // $operator='*'  * - any part Like %xx% | start - At start LIKE '%XXX' | end  - At end LIKE 'XXX%'
    public function get_contents_by_idnumber($operator='*',$idnumber,$courseid,$section=null) {
        global $DB, $CFG;
        $wsql="";
       
        if(!empty($courseid)){$wsql.=" AND cm.course=$courseid ";}
        if($section !=null && $section >=0 ){$wsql.=" AND s.section=$section ";}

        if(empty($operator)){return null;}
        if(empty($idnumber)){return null;}
        if($operator=='*'){$wsql.=" AND cm.idnumber LIKE '%".$idnumber."%' ";}
        else if($operator=='start'){$wsql.=" AND cm.idnumber LIKE '%".$idnumber."' ";}
        else if($operator=='end'){$wsql.=" AND cm.idnumber LIKE '".$idnumber."%' ";}
        $sql ="SELECT p.id,p.name,p.intro,p.content FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' $wsql ";
       
        $rows=$DB->get_records_sql($sql); 
        return $rows;
    }
    
      public function get_cmid_by_courseid($courseid) {
        global $DB, $CFG;
         $sql ="SELECT p.id,cm.id AS cmid FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module  INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' AND cm.course=$courseid ";
        $rows=$DB->get_records_sql($sql); 
        return $rows;
    }
    public function get_cmid_by_section($courseid,$section) {
        global $DB, $CFG;
         $sql ="SELECT p.id,cm.id AS cmid FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' AND cm.course=$courseid AND s.section=$section";
        $rows=$DB->get_records_sql($sql); 
        return $rows;
    }
    
     private function search_param($param) {
        $wsql="";
        if(isset($param['courseid']) && !empty($param['courseid'])){$wsql.=" AND cm.course=".$param['courseid']." ";}
        if(isset($param['section']) && $param['section']!=null){$wsql.=" AND s.section=".$param['section']." ";}
        if(isset($param['content'])  && !empty($param['content'])){$wsql.=" AND p.content LIKE '%".$param['content']."%' ";}
       return $wsql; 
    }
     public function search_count($param) {
        global $DB, $CFG;
        $wsql= $this->search_param($param);
        $sql ="SELECT  COUNT(l.id) AS countrecord  FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' $wsql ";
        $rows=$DB->get_record_sql($sql); 
        return $rows;
    }
    
    public function search($param) {
        global $DB, $CFG;
        $wsql= $this->search_param($param);
        $offset=0;
        $limit=100;
        if(isset($param['_offset']) && $param['_offset']!=null){$offset=$param['_offset'];}
        if(isset($param['_limit']) && $param['_limit']!=null){$limit=$param['_limit'];}
        $sql ="SELECT p.id,cm.id AS cmid,p.name,p.intro,p.content FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section INNER JOIN {$CFG->prefix}page p ON p.id=cm.instance WHERE m.name='page' $wsql ";
        $rows=$DB->get_records_sql($sql,null,$offset,$limit);
        return $rows;
    }
}

?>
