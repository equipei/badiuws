<?php
require_once("$CFG->dirroot/local/badiuws/lib/util.php");
class local_badiuws_lib_message {

    public function add($param) {
         $util=new local_badiuws_util();
         global $DB;
         
         $param=$util->addDefault($param,'fullmessageformat',0);
         $param=$util->addDefault($param,'component','moodle');
         $param=$util->addDefault($param,'eventtype','instantmessage');
         
         $useridfrom=$util->getVaueOfArray($param,'useridfrom');
         $usernamefrom=$this->get_user_fullname($useridfrom);
         $param=$util->addDefault($param,'subject',"new messsage from $usernamefrom");
         $param=$util->addDefault($param,'timecreated',time());
         
         $dto=  (object)$param;
         return $DB->insert_record('message', $dto);
    }
    
    public function get_user_fullname($userid) {
       global $DB, $CFG;
        $sql="SELECT firstname,lastname FROM {$CFG->prefix}user WHERE id=$userid";
        $r = $DB->get_record_sql($sql);
        if(empty($r)){ return ''; }
        $v=$r->firstname;
        $v=$v. " ".$r->lastname;
        return $v;
   } 
              
     private function general_search_wsql($param) {
          $util=new local_badiuws_util();
          $wsql="";
          $useridfrom=$util->getVaueOfArray($param,'useridfrom');
          if(!empty($useridfrom)){$wsql.=" AND m.useridfrom=$useridfrom ";}
          
           $useridto=$util->getVaueOfArray($param,'useridto');
          if(!empty($useridto)){$wsql=" AND m.useridto=$useridto ";}
          
          return  $wsql;
     }
     public function general_search_count($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
        $userid=$util->getVaueOfArray($param,'userid');
        
        $wsql=$this->general_search_wsql($param);
        $sql="SELECT COUNT(m.id) AS countrecord   FROM {$CFG->prefix}message m INNER JOIN {$CFG->prefix}user fu ON fu.id=m.useridfrom INNER JOIN {$CFG->prefix}user tu ON tu.id=m.useridto WHERE m.id > 0 $wsql";
        $r = $DB->get_record_sql($sql);
        $r =$r->countrecord;
        return $r;
   } 
   
    public function general_search($param) {
       $util=new local_badiuws_util();
        global $DB, $CFG;
        
      
        $offset=$util->getVaueOfArray($param,'offset');
        $limit=$util->getVaueOfArray($param,'limit');
        
         if(!empty($offset)){$offset=0;}
        if(!is_int((int)$offset)){$offset=0;}
        
        if(!empty($limit)){$limit=10;}
        if(!is_int((int)$limit)){$limit=10;}
        
        $wsql=$this->general_search_wsql($param);
        $sql="SELECT m.id,m.useridfrom, fu.firstname AS userfirstnamefrom, fu.lastname AS userlastnamefrom, fu.email AS useremailfrom, m.useridto, tu.firstname AS userfirstnameto, tu.lastname AS userlastnameto, tu.email AS useremailto,m.subject,m.fullmessage,m.timecreated FROM {$CFG->prefix}message m INNER JOIN {$CFG->prefix}user fu ON fu.id=m.useridfrom INNER JOIN {$CFG->prefix}user tu ON tu.id=m.useridto WHERE m.id > 0 $wsql ORDER BY m.timecreated DESC ";
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
   } 
   
}

?>
