<?php 
require_once("$CFG->dirroot/local/badiuws/lib/baserole.php");
require_once("$CFG->dirroot/local/badiuws/webservice/message/lib/messagelib.php");
class local_badiuws_webservice_message extends local_badiuws_baserole  {
    
      private $lib;
   function __construct() {
          parent::__construct();
           $this->lib=new local_badiuws_lib_message();
   }
   
     public function add() {
         global $DB;
          $useridto=$this->getParam()['useridto'];
          $useridfrom=$this->getParam()['useridfrom'];

          
         if(!isset($this->getParam()['useridfrom'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridfrom.undefined');}
         if(empty($this->getParam()['useridfrom'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridfrom.empty');}
         if(!is_int((int)$this->getParam()['useridfrom'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridfrom.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['useridfrom']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['useridfrom'].' not exist in database in the table user');}
        
         if(!isset($this->getParam()['useridto'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridto.undefined');}
         if(empty($this->getParam()['useridto'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridto.empty');}
         if(!is_int((int)$this->getParam()['useridto'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridto.isnotnumber');}
         if(!$DB->record_exists('user', array('id' => $this->getParam()['useridto']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.useridnotexist',$this->getParam()['useridto'].' not exist in database in the table user');}
        
        
         if(!isset($this->getParam()['fullmessage'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.fullmessage.undefined');}
         if(empty($this->getParam()['fullmessage'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.fullmessage.empty');}
       
         
         if(!empty($this->getParam()['timecreated'])){ 
             if(!is_int((int)$this->getParam()['timecreated'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.timecreated.isnotnumber');}
         }
         $result=null;
          try {
              $result=$this->lib->add($this->getParam());
              
         } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
        }
         
       
        return $result;
      }
     
}

$badiuwsdata=new local_badiuws_webservice_message();
?>
