<?php

class local_badiuws_utildata {
  
    function __construct() {
     
    }
    public function getVaueOfArray($array, $key, $pointseparator = false) {
        $value = null;
        if (!$pointseparator) {
            if (isset($array[$key])) {
                $value = $array[$key];
            }
            return $value;
        }

        //without separate point $pointseparator=true
        $pos = stripos($key, ".");

        if ($pos === false) {

            if (isset($array[$key])) {
                $value = $array[$key];
            }
            return $value;
        }

        //with point separator
        $listkey = explode(".", $key);

        $search = true;
        $maxloop = 100;
        $cont = 0;
        $currentvalue = null;
        while ($search) {

            $skey = null;

            if (array_key_exists($cont, $listkey)) {
                $skey = $listkey[$cont];
                if ($cont == 0) {
                    if (is_array($array) && array_key_exists($skey, $array)) {
                        $currentvalue = $array[$skey];
                    } else {
                        $currentvalue = null;
                    }
                } else {
                    if (is_array($currentvalue) && array_key_exists($skey, $currentvalue)) {
                        $currentvalue = $currentvalue[$skey];
                    } else {
                        $currentvalue = null;
                    }
                }
            } else {
                break;
            }

            if ($cont > 100) {
                break;
            }

            $cont++;
        }


        return $currentvalue;
    }
}
