<?php

class local_badiuws_response  { 
    
        public static  $REQUEST_ACCEPT   ="accept";
        public static  $REQUEST_DENIED   ="danied";
	private $status;
	private $info;
	private $message;
      
    function __construct() {
                $this->status='';
		$this->info='';
		$this->message='';
                $this->criptyidentify =0;
  }
       
      
      public function get() {
			$response=array();
			$response['status']=$this->status;
			$response['info']=$this->info;
			$response['message']=$this->message;
                       return $response;
      }
      public function accept($message,$info='',$senddata=true) {
            
			$this->setStatus(self::$REQUEST_ACCEPT);
			$this->setInfo($info);
                        $this->setMessage($message);
                        if($senddata){
                             $json = json_encode($this->get());
                             header("Content-Type: application/json;charset=utf-8");
                            echo $json; 
                            exit;
                        }
      }
       public function danied($info,$message='',$senddata=true) {
			$this->setStatus(self::$REQUEST_DENIED);
			$this->setInfo($info);
                        $this->setMessage($message);
                        if($senddata){
                             $json = json_encode($this->get());
                            header("Content-Type: application/json;charset=utf-8");
                             echo $json; 
                            exit;
                        }
      }
   
    public function getStatus() {
          return $this->status;
      }


      public function setStatus($status) {
          $this->status = $status;
      }
	  
	   public function getInfo() {
          return $this->info;
      }


      public function setInfo($info) {
          $this->info = $info;
      }
	   public function getMessage() {
          return $this->message;
      }


      public function setMessage($message) {
          $this->message = $message;
      }

     

}
