<?php 
require_once("$CFG->dirroot/local/badiuws/lib/response.php");
require_once("$CFG->dirroot/local/badiuws/lib/util.php");

class local_badiuws_error  {
     
    private $param = null;
    private $code;
    private $msg;
   
    public static $KEY_UNDEFINED ='badiu.moodle.ws.error.key.undefined';
    public static $KEY_EMPTY='badiu.moodle.ws.key.error.empty';
    public static $KEY_NOT_VALID='badiu.moodle.ws.error.key.notvalid';
    public static $TOKEN_UNDEFINED ='badiu.moodle.ws.error.token.undefined';
    public static $TOKEN_EMPTY='badiu.moodle.ws.error.token.empty';
    public static $TOKEN_NOT_VALID='badiu.moodle.error.ws.token.notvalid';
    public static $PARAM_ID_EMPTY='badiu.moodle.ws.error.�param.id.empty';
    
   function __construct($param) {
       $this->param=$param;
       $this->code=0;
       $this->msg=null;
     
       $this->check();
    }
   
    public function check() {
        $this->checkToken();
        $this->checkKey();
      
    }
    public function checkToken() {
        if(!array_key_exists('_token',$this->param)){$this->response(self::$TOKEN_UNDEFINED);}
        if(empty($this->param['_token'])){$this->response(self::$TOKEN_EMPTY);}
        $util=new local_badiuws_util();
        $exist=$util->existToken($this->param['_token']);
        if(!$exist){$this->response(self::$TOKEN_NOT_VALID);}
    }
     public function checkKey() {
         
        if(!array_key_exists('_key',$this->param)){$this->response(self::$KEY_UNDEFINED);}
        if(empty($this->param['_key'])){$this->response(self::$KEY_EMPTY);}
        
         
    }
   
    public function response($code) {
        $this->code=$code;
        $msg=utf8_encode($this->getMessage($code));
        $response=new local_badiuws_response();
        $response->setInfo($this->code);
        $response->setMessage($msg);
        $response->setStatus($response::$REQUEST_DENIED);
       
        $json = json_encode($response->get());
        header("Content-Type: application/json;charset=utf-8");
        echo $json; 
        exit;
    }
    public function getMessage($code) {
       
        if($code==self::$TOKEN_UNDEFINED){return 'Param token not defined';}
       
        if($code==self::$TOKEN_EMPTY){return 'Param token is empty';}
        
        if($code==self::$TOKEN_NOT_VALID){return 'Param token is not valid';}
        
         if($code==self::$KEY_UNDEFINED){return 'Param key not defined';}
        if($code==self::$KEY_EMPTY){return 'Param key is empty';}
        if($code==self::$KEY_NOT_VALID){return 'Param key is not valid';}
        
        return "";
    }
    
}
?>
