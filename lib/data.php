<?php 
require_once("$CFG->dirroot/local/badiuws/lib/response.php");
require_once("$CFG->dirroot/local/badiuws/lib/error.php");
class local_badiuws_data  {
   
    private $param = null; 
    private $folder;
    private $file;
    private $function;
    private $key;
    private $iskeyvalid;
    private $response;
   function __construct($param) {
       $this->param=$param;
       $this->folder=null;
       $this->file=null;
       $this->function=null;
       $this->key=null;
       $this->iskeyvalid=false;
       $this->response=new local_badiuws_response();
       $this->init();
   }
   function init() {
      if(isset($this->param['_key'])){$this->key=$this->param['_key'];}
      $this->iskeyvalid=$this->isKeyValid($this->key);
      if($this->iskeyvalid){
           $p=explode('.',$this->key);
          
           $this->folder=$p[0];
           $this->file=$p[1];
           $this->function=$p[2];
      }

    }
    
   function exec() {
        global $CFG;
         $folder=$this->folder;
        $file=$this->file;
        $function=$this->function;
       
        $filepath="$CFG->dirroot/local/badiuws/webservice/$folder/$file.php";
        if(file_exists($filepath)){
            global $badiuwsdata;
            require_once($filepath);
           
           $existfunction=method_exists($badiuwsdata,$function);
            if(!$existfunction){
                $info=local_badiuws_error::$KEY_NOT_VALID;
                $message="function not found";
                $this->response->danied($info,$message);
            }else{
                 $param=$this->param;
                  unset($param['_key']);
                  unset($param['_token']);
                  
                 $badiuwsdata->setParam($param);
                 $result=$badiuwsdata->$function();
                 $this->response->accept($result);
            }
           
        }else{
            $info=local_badiuws_error::$KEY_NOT_VALID;
            $message="file not found";
            $this->response->danied($info,$message);
        }
        
        
    }
    
    function isKeyValid($key) {
       
        if(empty($key)){return false;}
        
        $count=substr_count($key, '.');
        
        if($count==2){
              $p=explode('.',$key);
             if(isset($p[0]) && !empty($p[0])
                && isset($p[1]) &&  !empty($p[1])
                && isset($p[2]) &&  !empty($p[2])){return true;}
        }
        return false;
    }
     public function response($code) {
        $this->code=$code;
        $msg=utf8_encode($this->getMessage($code));
        $response=new local_badiuws_response();
        $response->danied($this->code,$msg);
    }
      
   
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
}
?>
