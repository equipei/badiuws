<?php 
require_once("$CFG->dirroot/local/badiuws/lib/response.php");
require_once("$CFG->dirroot/local/badiuws/lib/baselib.php");
class local_badiuws_baserole  extends local_badiuws_baselib{
    
    private $param = null;
    private $response;
    function __construct() {
		parent::__construct();
        $this->response=new local_badiuws_response();
		
    }
    
    public function login() {
            global $DB;
           $user = $DB->get_record('user', array('username'=>'admin')); 
           complete_user_login($user);
      }
      
   function getPaginationOffset() {
       $offset=0;
       if(isset($this->getParam()['offset']) && (int)$this->getParam()['offset'] >=0 ){
           $offset=(int)$this->getParam()['offset'];
      }
     return $offset;
    }
     function getPaginationLimit() {
       $limit=10;
       if(isset($this->getParam()['limit']) && (int)$this->getParam()['limit'] >=0 ){
           $limit=(int)$this->getParam()['limit'];
      }
     return $limit;
    }
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
 
    function getResponse() {
        return $this->response;
    }

    function setResponse($response) {
        $this->response = $response;
    }


 
}


?>
