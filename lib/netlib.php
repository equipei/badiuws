<?php
require_once($CFG->dirroot.'/local/badiuws/lib/pluginconfig.php'); 
class local_badiuws_netlib  {

      /**
     * @var string
     */
    private $token;
	
		
    function __construct(){
		$this->initConfig();
	}
	
	public function initConfig() {
		
		//check config token
		$plugin=new local_badiuws_pluginconfig('local_badiuws'); 
		$usebadiunetoken=$plugin->getValue('usebadiunetoken');
		
		if(!empty($usebadiunetoken)){
			//defautl token
			if($usebadiunetoken=='defaultoken'){
				 $bnetplugin=new local_badiuws_pluginconfig('local_badiunet');   
                 $this->token=$bnetplugin->getValue('servicetoken');
			}else{
				//appsservice token
				global $CFG;
				$filepluginbadiunetsserverdblib=$CFG->dirroot.'/local/badiunet/app/sserver/dblib.php';
				if(file_exists($filepluginbadiunetsserverdblib)){
					require_once($filepluginbadiunetsserverdblib);
					$dblib = new local_badiunet_app_sserver_dblib();
					$this->token=$dblib->get_comun_by_servicekeyinstance($usebadiunetoken,'servicetoken');
				}
			}
		} //end if(!empty($usebadiunetoken))
		//local token
		else{
			$this->token=$plugin->getValue('wstoken');
        }
		$this->token=$this->clean($this->token);
	}
	/*public function initConfig() {
		$plugin=new local_badiuws_pluginconfig('local_badiuws');  
		$this->token=$plugin->getValue('wstoken');
                $this->token=$this->clean($this->token);
                
                
                $usebadiunetoken=$plugin->getValue('usebadiunetoken');
                if($usebadiunetoken){
                      $plugin=new local_badiuws_pluginconfig('local_badiunet');   
                      $this->token=$plugin->getValue('servicetoken');
                 }
         }*/
   public function clean($str) {
            if(!empty($str)){
                $str=trim($str);
                $str= str_replace("\\t",'',$str);
                $str= str_replace(" ",'',$str);
            }
           return $str; 
        }
       public function getToken() {
          return $this->token;
      }

      public function setToken($token) {
          $this->token = $token;
      }
	  
	

}
