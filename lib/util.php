<?php 
require_once("$CFG->dirroot/local/badiuws/lib/netlib.php");
class local_badiuws_util  {
   
   function __construct() {
  
   }
   public function getVaueOfArray($array,$key) {
           $value=null;
            if(array_key_exists($key,$array)){$value=$array[$key];}
	   return $value;
	}
        
   public function addDefault($array,$key,$value) {
       if(!is_array($array)){return $array;}
       if(!array_key_exists($key,$array)){$array[$key]=$value;}
       return $array;
   }
  
   function convertData($data,$outarray=false) {
        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $data);
        $oresult = json_decode($json_without_bigints, $outarray);
        return $oresult;
    }
   public function existToken($token) {
         $exist=false;
         $netlib=new local_badiuws_netlib();
         $defaulttoken=$netlib->getToken();
         if($token==$defaulttoken){$exist=true;}
        return $exist;
   }
   
           
}
?>
