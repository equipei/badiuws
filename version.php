<?php

defined('MOODLE_INTERNAL') || die;
 
$plugin->version = 2023080700; // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires = 2012112900; // Requires this Moodle version
$plugin->component = 'local_badiuws'; // Full name of the plugin (used for diagnostics)
